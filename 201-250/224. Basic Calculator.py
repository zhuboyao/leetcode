#Hard

#Very Important
#Time Complexity: O(N)
#Space Complexity: O(N)
class Solution:
    def calculate(self, s: str) -> int:
        stack = []
        sign = 1
        res = 0
        i = 0
        while i < len(s):
            if s[i].isdigit():
                num = int(s[i])
                while i+1 < len(s) and s[i+1].isdigit():
                    num = num*10 + int(s[i+1])
                    i += 1
                res += num*sign
            elif s[i] == "+":
                sign = 1
            elif s[i] == "-":
                sign = -1
            elif s[i] == "(":
                stack.append(res)
                stack.append(sign)
                res = 0
                sign = 1
            elif s[i] == ")":
                res = res*stack.pop() + stack.pop()
            i += 1
        return res

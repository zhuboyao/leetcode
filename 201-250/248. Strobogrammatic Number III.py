#Hard


class Solution:
    def strobogrammaticInRange(self, low: str, high: str) -> int:
        res = 0
        lists = []
        for i in range(len(low),len(high)+1):
            lists = lists + self.helper(i,i)
        for num in lists:
            if len(num) == len(low) and num < low or len(num) == len(high) and num > high:
                continue
            res += 1
        return res
    
    def helper(self,n,m):
        if n == 0:
            return [""]
        if n == 1:
            return ["1","8","0"]
        res = []
        center = self.helper(n-2,m)
        for i in range(len(center)):
            s = center[i]
            if n != m:
                res.append("0"+s+"0")
            res.append("1"+s+"1")
            res.append("8"+s+"8")
            res.append("6"+s+"9")
            res.append("9"+s+"6")
        return res

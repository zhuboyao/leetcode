#Medium
#Time: O(9!K/(9-K)!)
#Space: O(K)

class Solution:
    def combinationSum3(self, k: int, n: int) -> List[List[int]]:
        result = []
        self.backtrack(result, n, [], 0,k)
        return result
    
    def backtrack(self,result,remain,comb,next_start,k):
        if remain == 0 and len(comb) == k:
            result.append(list(comb))
            return
        elif remain < 0 or len(comb) == k:
            return
        for i in range(next_start,9):
            comb.append(i+1)
            self.backtrack(result,remain-i-1,comb,i+1,k)
            comb.pop()
            

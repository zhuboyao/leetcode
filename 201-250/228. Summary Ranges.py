#Easy
#Time Complexity: O(N)
#Space Complexity: O(1)

class Solution:
    def summaryRanges(self, nums: List[int]) -> List[str]:
        res = []
        i = 0
        while i<len(nums):
            num = nums[i]
            while i<len(nums)-1 and nums[i]+1 == nums[i+1]:
                i += 1
            if num != nums[i]:
                s_num = str(num)
                s_end = str(nums[i])
                res.append(s_num+"->"+s_end)
            else:
                res.append(str(num))
            i += 1
        return res

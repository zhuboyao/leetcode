#Hard

#Not that important
#Time Limit Exceeded
#Time Complexity: O(NlogN)
#Space Complexity: O(logN)
class Solution:
    def countDigitOne(self, n: int) -> int:
        count = 0
        for i in range(1,n+1):
            s = str(i)
            count += s.count("1")
        return count

# No. of 1s at one's Position (n/10) + (n%10!=0)
# No. of 1s at ten's Position (n/100)*10 + min(max(n%100 - 10 +1,0),10)
# No. of 1s at Hundread's Dosition (n/1000)*100 + min(max(n%1000 - 100  +1,0),100)
class Solution:
    def countDigitOne(self, n: int) -> int:
        res = 0
        cur = 1
        while cur <= n:
            res += ((n//(cur*10))*cur)+min(max(n % (cur*10)-cur+1, 0), cur)
            cur *= 10
        return res
#Time Complexity: O(logN)
#Space Complexity: O(1)

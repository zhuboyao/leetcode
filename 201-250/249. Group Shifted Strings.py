#Medium

#Time Complexity: O(N*K)
#Space Complexity: O(N*K)
class Solution:
    def groupStrings(self, strings: List[str]) -> List[List[str]]:
        groups = collections.defaultdict(list)
        for string in strings:
            hash_key = self.get_hash(string)
            groups[hash_key].append(string)
        return list(groups.values())
    
    def get_hash(self,string):
        key = []
        for a, b in zip(string,string[1:]):
            key.append(chr((ord(b)-ord(a))%26+ord("a")))
        return "".join(key)
        

#Medium

#DFS
#Time: O(|E|+|V|), |V| is the number of courses, |E| is the number of dependencies
#Space: O(|E|+|V|)
class Solution:
    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        courseDict = defaultdict(list)
        for relation in prerequisites:
            nextCourse, prevCourse = relation[0], relation[1]
            courseDict[prevCourse].append(nextCourse)
        
        checked = [False]*numCourses
        path = [False]*numCourses
        
        for currCourse in range(numCourses):
            if self.isCyclic(currCourse, courseDict, checked, path):
                return False
        return True
    
    def isCyclic(self,currCourse, courseDict, checked, path):
        if checked[currCourse]: # this node has been checked, no cycle would be formed with this node.
            return False
        if path[currCourse]: # came across a marked node in the path, cyclic !
            return True
        path[currCourse] = True
        ret = False
        for child in courseDict[currCourse]:
            ret = self.isCyclic(child, courseDict, checked, path)
            if ret:
                break
        path[currCourse] = False
        checked[currCourse] = True
        return ret



#BFS
class Solution:
    def canFinish(self, numCourses: int, prerequisites: List[List[int]]) -> bool:
        edges = {i: [] for i in range(numCourses)}
        indegrees = [0 for i in range(numCourses)]
        for i, j in prerequisites:
            edges[j].append(i)
            indegrees[i] += 1 #number of line direct to i
        
        queue,count = deque([]), 0
        
        for i in range(numCourses):
            if indegrees[i] == 0:
                queue.append(i)
        
        while queue:
            node = queue.popleft()
            count += 1
            for x in edges[node]:
                indegrees[x] -= 1
                if indegrees[x] == 0:
                    queue.append(x)
        return count == numCourses

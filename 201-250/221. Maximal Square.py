 #Medium
 #DP
 #Time Complexity: O(MN)
 #Space Complexity: O(MN)
 class Solution:
    def maximalSquare(self, matrix: List[List[str]]) -> int:
        if not matrix:
            return 0
        M = len(matrix)
        N = len(matrix[0])
        maxsqlen = 0
        dp = [[0]*(N+1) for _ in range(M+1)]
        for i in range(1,M+1):
            for j in range(1,N+1):
                if int(matrix[i-1][j-1]) == 1:
                    dp[i][j] = min(dp[i][j-1],dp[i-1][j],dp[i-1][j-1])+1
                    maxsqlen = max(maxsqlen,dp[i][j])
        return maxsqlen*maxsqlen

#Optimized DP
#Space: O(N)
class Solution:
    def maximalSquare(self, matrix: List[List[str]]) -> int:
        if not matrix:
            return 0
        M = len(matrix)
        N = len(matrix[0])
        maxsqlen = 0
        prev = 0
        dp = [0]*(N+1)
        for i in range(1,M+1):
            for j in range(1,N+1):
                temp = dp[j]
                if matrix[i-1][j-1] == "1":
                    dp[j] = min(dp[j-1],prev,dp[j])+1
                    maxsqlen = max(maxsqlen,dp[j])
                else:
                    dp[j] = 0
                prev = temp
        return maxsqlen*maxsqlen

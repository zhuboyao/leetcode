#Medium

#Time Complexity: O(N)
#Space Complexity: O(H)
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def countUnivalSubtrees(self, root: Optional[TreeNode]) -> int:
        self.count = 0
        self.is_valid_part(root)
        return self.count
    
    def is_valid_part(self,root):
        if not root:
            return True
        left = self.is_valid_part(root.left)
        right = self.is_valid_part(root.right)
        if left == True and right == True:
            if root.left and root.val != root.left.val:
                return False
            if root.right and root.val != root.right.val:
                return False
            self.count += 1
            return True
        return False

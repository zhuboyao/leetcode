#Easy
#Similar to 217, 220

#Time Complexity: O(N)
#Space Complexity: O(min(n,k))

class Solution:
    def containsNearbyDuplicate(self, nums: List[int], k: int) -> bool:
        HashMap = {}
        for i in range(len(nums)):
            if nums[i] in HashMap:
                if i - HashMap[nums[i]] <= k:
                    return True
            HashMap[nums[i]] = i
        return False

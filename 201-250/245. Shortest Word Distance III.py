#Easy

#Time: O(N)
#Space: O(1)
class Solution:
    def shortestWordDistance(self, wordsDict: List[str], word1: str, word2: str) -> int:
        res = len(wordsDict)
        i1 = -1
        i2 = -1
        isSame = word1==word2
        
        for i in range(len(wordsDict)):
            if wordsDict[i] == word1:
                i1 = i
            if wordsDict[i] == word2:
                if isSame:
                    i1 = i2
                i2 = i
            if i1!=-1 and i2!=-1:
                res = min(res,abs(i1-i2))
        return res

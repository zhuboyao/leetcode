#Hard
#Using Trie Tree

#Time: O(M(4*3^{L-1})), M is the number of cells, L is the maximum length of words
#Space: O(N), N is the total number of letters in dictionary.
class Solution:
    def findWords(self, board: List[List[str]], words: List[str]) -> List[str]:
        self.WORD_KEY = '$'
        trie = {}
        for word in words:
            node = trie
            for letter in word:
                node = node.setdefault(letter,{})
            node[self.WORD_KEY] = word
        
        rowNum = len(board)
        colNum = len(board[0])
        
        matchedWords = []
        
        for row in range(rowNum):
            for col in range(colNum):
                if board[row][col] in trie:
                    self.backtracking(board,row,col,trie,matchedWords)
        
        return matchedWords
    
    def backtracking(self,board,row,col,parent,matchedWords):
        rowNum = len(board)
        colNum = len(board[0])
        letter = board[row][col]
        currNode = parent[letter]
        word_match = currNode.pop(self.WORD_KEY,False)
        if word_match:
            matchedWords.append(word_match)
        board[row][col] = "#"
        for (rowOffset, colOffset) in [(-1,0),(0,1),(1,0),(0,-1)]:
            newRow, newCol = row+rowOffset, col+colOffset
            if newRow<0 or newRow>=rowNum or newCol<0 or newCol>=colNum:
                continue
            if not board[newRow][newCol] in currNode:
                continue
            self.backtracking(board,newRow,newCol,currNode,matchedWords)
        board[row][col] = letter
        if not currNode:
            parent.pop(letter)


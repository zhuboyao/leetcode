#Medium

#Time Complexity: O(N)
#Space Complexity: O(d) = O(logN)
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def countNodes(self, root: Optional[TreeNode]) -> int:
        if root:
            return 1 + self.countNodes(root.right) + self.countNodes(root.left)
        return 0



#Time Complexity: O(d^2) = O(log^2N), where d is a tree depth
#Space Complexity: O(1)
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def countNodes(self, root: Optional[TreeNode]) -> int:
        if not root:
            return 0
        d = self.compute_depth(root)
        if d == 0:
            return 1
        left, right = 1, 2**d-1 #enumerate from 0 to 2**d-1 in all levels but the last one
        while left <= right:
            pivot = (left+right)//2
            if self.exists(pivot,d,root):
                left = pivot + 1
            else:
                right = pivot - 1
        return (2**d-1) + left
    
    def compute_depth(self,root):
        d = 0
        while root.left:
            root = root.left
            d += 1
        return d
    
    def exists(self,idx,d,node):
        left, right = 0, 2**d-1
        for _ in range(d):
            pivot = (left+right)//2
            if idx<=pivot:
                node = node.left
                right = pivot
            else:
                node = node.right
                left = pivot + 1
        return node is not None
            
#Tree has 2^d-1 nodes on all levels except the last one.
#Last level d: 1<=nodes<=2^d
#Total number of nodes = 2^d-1+last_level_nodes

#Use binary search to check nodes existence in the last level
#Use binary search to construct the sequence of moves to the leaf


#Time Complexity: O(d^2) = O(log^2N), where d is a tree depth
#Space Complexity: O(n)
class Solution:
    def countNodes(self, root: Optional[TreeNode]) -> int:
        left = self.leftDepth(root)
        right = self.rightDepth(root)
        if left == right:
            return (1<<left)-1
        return 1+self.countNodes(root.left) + self.countNodes(root.right)
        
    def leftDepth(self,root):
        res = 0
        while root:
            root = root.left
            res += 1
        return res
    def rightDepth(self,root):
        res = 0
        while root:
            root = root.right
            res += 1
        return res
    

#Medium
#Important, but less important than 211

class Trie:

    def __init__(self):
        self.words = dict()
        self.prefixes = dict()

    def insert(self, word: str) -> None:
        for i in range(len(word)):
            self.prefixes[hash(word[:i+1])] = word[:i+1]
        self.words[hash(word)] = word

    def search(self, word: str) -> bool:
        return hash(word) in self.words.keys()

    def startsWith(self, prefix: str) -> bool:
        return hash(prefix) in self.prefixes.keys()


# Your Trie object will be instantiated and called as such:
# obj = Trie()
# obj.insert(word)
# param_2 = obj.search(word)
# param_3 = obj.startsWith(prefix)

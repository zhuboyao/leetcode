#Easy

#Time: O(NlogN)
#Space: O(N)
class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False
        ls = list(s)
        lt = list(t)
        print(lt)
        ls.sort()
        lt.sort()
        return ls == lt


#Time: O(N)
#Space: O(1)
class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False
        
        count = [0]*26
        for i in range(len(s)):
            count[ord(s[i])-ord('a')] += 1
            count[ord(t[i])-ord('a')] -= 1
        for num in count:
            if num! == 0:
                return False
        return True

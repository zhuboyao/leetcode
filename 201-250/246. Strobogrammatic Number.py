#Easy

#Time: O(N)
#Space: O(N)
class Solution:
    def isStrobogrammatic(self, num: str) -> bool:
        rotated_digits = {"0":"0","1":"1","8":"8","6":"9","9":"6"}
        rotated_string_builder = []
        for c in reversed(num):
            if c not in rotated_digits:
                return False
            rotated_string_builder.append(rotated_digits[c])
        rotated_string = "".join(rotated_string_builder)
        return rotated_string == num

#two pointer
#Time: O(N)
#Space: O(1)
class Solution:
    def isStrobogrammatic(self, num: str) -> bool:
        rotated_digits = {'0': '0', '1': '1', '8': '8', '6': '9', '9': '6'}
        left = 0
        right = len(num)-1
        while left <= right:
            if num[left] not in rotated_digits or rotated_digits[num[left]] != num[right]:
                return False
            left += 1
            right -= 1
        return True

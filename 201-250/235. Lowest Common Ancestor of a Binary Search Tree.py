#Easy

#Recursive
#Time Complexity: O(N)
#Space Complexity: O(N)
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        parent = root.val
        p_val = p.val
        q_val = q.val
        if p_val > parent and q_val > parent:
            return self.lowestCommonAncestor(root.right,p,q)
        elif p_val < parent and q_val < parent:
            return self.lowestCommonAncestor(root.left,p,q)
        else:
            return root

#Iterative
#Time Complexity: O(N)
#Space Complexity: O(1)
class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        p_val = p.val
        q_val = q.val
        node = root
        while node:
            parent = node.val
            if p_val > parent and q_val > parent:
                node = node.right
            elif p_val < parent and q_val < parent:
                node = node.left
            else:
                return node

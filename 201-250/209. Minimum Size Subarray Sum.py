#Medium

#Time Complexity: O(N)
#Space Complexity: O(1)
class Solution:
    def minSubArrayLen(self, target: int, nums: List[int]) -> int:
        n = len(nums)
        ans = sys.maxsize
        left = 0
        Sum = 0
        for i in range(n):
            Sum += nums[i]
            while Sum >= target:
                ans = min(ans,i+1-left)
                Sum -= nums[left]
                left += 1
        if ans != sys.maxsize:
            return ans
        else:
            return 0

#We could keep 2 pointer,one for the start and another for the end of the current subarray, and make optimal moves so as to keep the sum greater than ss as well as maintain the lowest size possible.

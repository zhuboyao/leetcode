#Medium

#Time: O(E+V)
#Space: O(E+V)
class Solution:
    def findOrder(self, numCourses: int, prerequisites: List[List[int]]) -> List[int]:
        listofPrereq = [[] for _ in range(numCourses)]
        for course, Prereq in prerequisites:
            listofPrereq[course].append(Prereq)
        # -1 never go through, 0 hold 1 take course
        visited = [-1 for _ in range(numCourses)]
        res = []
        for course in range(numCourses):
            if not self.DFS(listofPrereq, course, visited, res):
                return []
        return res
    
    def DFS(self,listofPrereq,course,visited,res):
        if visited[course] == 1:
            return True
        if visited[course] == 0:
            return False
        visited[course] = 0
        for Prereq in listofPrereq[course]:
            if not self.DFS(listofPrereq,Prereq,visited,res):
                return False
        visited[course] = 1
        res.append(course)
        return True



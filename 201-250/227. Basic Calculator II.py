#Medium


class Solution:
    def calculate(self, s: str) -> int:
        if not s:
            return 0
        stack = []
        res = 0
        sign = "+"
        num = 0
        i = 0
        while i < len(s):
            if s[i].isdigit():
                num = ord(s[i]) - ord('0')
                while i+1 < len(s) and s[i+1].isdigit():
                    num = num*10 + ord(s[i+1]) - ord("0")
                    i += 1
            if not s[i].isdigit() and s[i] != " " or i == len(s)-1:
                if sign == "+":
                    stack.append(num)
                elif sign == "-":
                    stack.append(-num)
                elif sign == "*":
                    stack[-1] = stack[-1]*num
                elif sign == "/" and abs(stack[-1]) > 0:
                    stack[-1] = (abs(stack[-1])//num)*(stack[-1]//abs(stack[-1]))
                sign = s[i]
            i += 1
        for v in stack:
            res += v
        return res



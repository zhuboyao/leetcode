#Easy

#Time Complexity: O(logN)
#Space Complexity: O(logN), a measure of what numbers we're putting in the Hashset
#Idea: Detect Cycles witha a HashSet
class Solution:
    def isHappy(self, n: int) -> bool:
        seen = set()
        while n!=1 and n not in seen:
            seen.add(n)
            n = self.get_next(n)
        return n==1
    
    def get_next(self,n):
        total = 0
        while n>0:
            digit = n%10
            n = n//10
            total += digit**2
        return total


#Time: O(logN)
#Space: O(1)
class Solution:
    def isHappy(self, n: int) -> bool:
        slow = n
        fast = self.get_next(n)
        while fast != 1 and slow != fast:
            slow = self.get_next(slow)
            fast = self.get_next(self.get_next(fast))
        return fast == 1
    
    def get_next(self,n):
        total = 0
        while n>0:
            digit = n%10
            n = n//10
            total += digit**2
        return total

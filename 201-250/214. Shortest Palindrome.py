#Hard

#KMP don't know
#Time Complexity: O(N)
#Space Complexity: O(N)
class Solution:
    def shortestPalindrome(self, s: str) -> str:
        if not s:
            return s
        A = s + s[::-1]
        prefix = self.getPrefix(A)
        print(prefix)
        i = prefix[-1]
        while i >= len(s):
            i = prefix[i]
        return s[i+1:][::-1]+s
    
    def getPrefix(self,pattern):
        prefix = [-1] * len(pattern)
        j = -1
        for i in range(1,len(pattern)):
            while j > -1 and pattern[j+1] != pattern[i]:
                j = prefix[j]
            if pattern[j+1] == pattern[i]:
                j += 1
            prefix[i] = j
        return prefix


#Time: 0(N^2)
#Time Limit Exceeded
class Solution:
    def shortestPalindrome(self, s: str) -> str:
        n = len(s)
        i, j, end = 0, n-1, n-1
        while i < j:
            if s[i] == s[j]:
                i += 1
                j -= 1
            else:
                i = 0
                end -= 1
                j = end
        s0 = s[end+1:][::-1]
        return s0+s

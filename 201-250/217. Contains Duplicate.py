#Easy
# 219 220
#Time: O(N)
#Space: O(N)
class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        HashSet = {}
        for num in nums:
            if num not in HashSet.keys():
                HashSet[num] = 1
            else:
                return True
        return False

#alternative: 1. Brutal force, 2 sort

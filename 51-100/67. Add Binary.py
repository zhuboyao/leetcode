#Easy
Time complexity :O(N+M),N, M are lengths of the input strings a and b.

Space complexity :O(max(N,M)) to keep the answer.
class Solution:
    def addBinary(self, a: str, b: str) -> str:
        i = len(a)-1
        j = len(b)-1
        remainder = 0
        res = ""
        while i>=0 or j>=0:
            Sum = remainder
            if i>=0:
                Sum += int(a[i])
                i += -1
            if j>=0:
                Sum += int(b[j])
                j += -1
            remainder = Sum//2
            res = str(Sum%2)+res
        if remainder:
            res = str(remainder)+res
        return res
            

#Medium

#Time: O(N)
#Space: O(N)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isValidBST(self, root: Optional[TreeNode]) -> bool:
        return self.validate(root)
    
    def validate(self,node,low=-math.inf,high=math.inf):
        if not node:
            return True
        if node.val <= low or node.val >= high:
            return False
        return self.validate(node.right,node.val,high) and self.validate(node.left,low,node.val)
        

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isValidBST(self, root: Optional[TreeNode]) -> bool:
        def inorder(root):
            return inorder(root.left) + [root.val] + inorder(root.right) if root else []
        res = inorder(root)
        for i in range(1,len(res)):
            if res[i] <= res[i-1]:
                return False
        return True
        

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isValidBST(self, root: Optional[TreeNode]) -> bool:
        if not root:
            return True
        stack=[(root,-math.inf, math.inf)]
        while stack:
            root, lower, upper = stack.pop()
            if not root:
                continue
            val = root.val
            if val <= lower or val >= upper:
                return False
            stack.append((root.right,val,upper))
            stack.append((root.left,lower,val))
        return True

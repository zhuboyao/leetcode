#Mediu
#followup 26
#in this problem 80, at most twice for each element

#Time O(N)
#Space O(1)


class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        n = len(nums)
        if n<=2:
            return n
        index = 2
        for i in range(index,n):
            if nums[i] != nums[index-2]:
                nums[index] = nums[i]
                index += 1
        return index
    

#Medium
#Time: O(n^2)
#Space: O(1)
class Solution:
    def generateMatrix(self, n: int) -> List[List[int]]:
        matrix = [[0]*n for _ in range(n)]
        rowBegin = 0
        colBegin = 0
        rowEnd = len(matrix)-1
        colEnd = len(matrix[0])-1
        k = 1
        while rowBegin<=rowEnd and colBegin<=colEnd:
            for i in range(colBegin,colEnd+1):
                matrix[rowBegin][i] = k
                k += 1
            rowBegin += 1
            for i in range(rowBegin,rowEnd+1):
                matrix[i][colEnd] = k
                k += 1
            colEnd -= 1
            for i in range(colEnd,colBegin-1,-1):
                matrix[rowEnd][i] = k
                k += 1
            rowEnd -= 1
            for i in range(rowEnd,rowBegin-1,-1):
                matrix[i][colBegin] = k
                k += 1
            colBegin += 1
        return matrix



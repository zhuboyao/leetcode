#Hard
#very important
#Using Stack

#Time complexity : O(n). n numbers are pushed and popped.
#Space complexity : O(n). Stack is used.
class Solution:
    def largestRectangleArea(self, heights: List[int]) -> int:
        stack = [-1]
        max_area = 0
        for i in range(len(heights)):
            while stack[-1] != -1 and heights[stack[-1]] >= heights[i]:
                current_height = heights[stack.pop()]
                current_width = i - stack[-1] - 1
                max_area = max(max_area,current_height*current_width)
            stack.append(i)
            
        while stack[-1] != -1:
            current_height = heights[stack.pop()]
            current_width = len(heights)-stack[-1]-1
            max_area = max(max_area,current_height*current_width)
        return max_area

#Idea: push -1 on the stack to mark the end.  We start with the leftmost bar and keep pushing the current bar's INDEX onto the stack until we get two successive numbers in descending order.  We start popping the numbers from the stack until we hit a number stack[j] on the stack such that a[stack[j]] <= a[i].  Every time we pop, we find out the area jof rectangle formed using the current element as the height of the rectangle and the difference between the current element's index pointed to in the original array and element stack[top-1]-1 as the width.

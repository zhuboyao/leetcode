#Medium
#Time complexity :O(mn). We traverse the entire matrix once.
#Space complexity :O(1). No extra space is used.


class Solution:
    def minPathSum(self, grid: List[List[int]]) -> int:
        m = len(grid)
        n = len(grid[0])
        for i in range(m):
            for j in range(n):
                if i == 0 and j != 0: #first row
                    grid[i][j] = grid[i][j] + grid[i][j-1]
                if i != 0 and j == 0: #first col
                    grid[i][j] = grid[i][j] + grid[i-1][j]
                if i != 0 and j != 0:
                    grid[i][j] += min(grid[i-1][j],grid[i][j-1])
        return grid[m-1][n-1]

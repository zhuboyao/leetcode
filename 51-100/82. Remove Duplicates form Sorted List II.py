#Medium
#Time Complexity: O(N)
#Space Complexity: O(1)



# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def deleteDuplicates(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if not head or not head.next:
            return head
        dummy = ListNode(0)
        dummy.next = head
        p = dummy
        while p.next and p.next.next:
            if p.next.val == p.next.next.val:
                sameNum = p.next.val
                while p.next and p.next.val == sameNum:
                    p.next = p.next.next
            else:
                p = p.next
        return dummy.next

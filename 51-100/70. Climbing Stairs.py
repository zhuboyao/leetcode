#Easy
#Very important, DP

#Time limit exceeded
#Time O(2^n)
#Space O(n)
class Solution:
    def climbStairs(self, n: int) -> int:
        if n <= 2:
            return n
        return self.climbStairs(n-1) + self.climbStairs(n-2)

#Time O(n)
#Space O(1)
class Solution:
    def climbStairs(self, n: int) -> int:
        if n <= 1:
            return 1
        oneStep = twoStep = 1
        res = 0
        for i in range(2,n+1):
            res = oneStep + twoStep
            twoStep = oneStep
            oneStep = res
        return res

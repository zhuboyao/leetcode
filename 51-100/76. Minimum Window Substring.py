#Hard
#Very very important!!!!
#Sliding windows

#idea: in any sliding window based problem we have two pointers. "i" pointer whose job is to expand the current window and "j" pointer howse job is to contract a given window.

#Time Complexity: O(n)
#Space Complexity: O(1)
class Solution:
    def minWindow(self, s: str, t: str) -> str:
        cnt = [0]*128
        for c in t:
            cnt[ord(c)] += 1
        start = 0
        total = len(t)
        minimum = sys.maxsize
        j=0
        for i in range(len(s)):
            cnt[ord(s[i])] -= 1
            if cnt[ord(s[i])] >= 0:
                total -= 1
            while total == 0:
                if i-j+1 < minimum:
                    minimum = i-j+1
                    start = j
                cnt[ord(s[j])] += 1
                if cnt[ord(s[j])] > 0:
                    total += 1
                j += 1
        if minimum == sys.maxsize:
            return ""
        return s[start:start+minimum]

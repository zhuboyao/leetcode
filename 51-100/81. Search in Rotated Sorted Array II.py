#Medium
#Time Complexity: O(logN)
#Space Complexity: O(1)

class Solution:
    def search(self, nums: List[int], target: int) -> bool:
        start, end = 0, len(nums)-1
        while start <= end:
            mid = (start + end)//2
            if nums[mid] == target:
                return True
            if nums[start] < nums[mid]:
                if nums[start] <= target < nums[mid]:
                    end = mid-1
                else:
                    start = mid+1
            elif nums[start] > nums[mid]:
                if nums[mid] < target <= nums[end]:
                    start = mid + 1
                else:
                    end = mid - 1
            else:
                start += 1
        return False


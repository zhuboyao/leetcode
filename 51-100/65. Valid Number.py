#Hard
#Time O(N)
#Space O(1)
class Solution:
    def isNumber(self, s: str) -> bool:
        s = s.strip()
        numSeen = False
        ptSeen = False
        eSeen = False
        numAfterE = False
        for i in range(len(s)):
            if s[i] >= '0' and s[i] <= '9':
                numSeen = True
                if eSeen:
                    numAfterE = True
            elif s[i] == '.':
                if eSeen or ptSeen:
                    return False
                ptSeen = True
            elif s[i] in 'eE' :
                if eSeen or not numSeen:
                    return False
                eSeen = True
            elif s[i] == '+' or s[i] == '-':
                if i != 0 and s[i-1] not in "eE":
                    return False
            else:
                return False
        return (numSeen and numAfterE) or (numSeen and not eSeen)

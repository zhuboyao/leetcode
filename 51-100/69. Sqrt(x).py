#Easy

#Similar to 367 Valid Perfect Square
#Time complexity: O(logN)
#Space complexity: O(1)
class Solution:
    def mySqrt(self, x: int) -> int:
        if x < 2:
            return x
        low, high = 2, x//2
        while low <= high:
            mid = (high-low)//2 + low
            prod = mid*mid
            if prod == x:
                return mid
            elif prod < x:
                low = mid+1
            else:
                high = mid-1
        return high
            

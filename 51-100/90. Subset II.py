#Easy
#Similar to 78 Subset
#Time: O(N*2^N)
#Space: O(N)

class Solution:
    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]:
        nums.sort()
        res = []
        self.dfs(res,[],nums,0)
        return res
    
    def dfs(self,res,path,nums,index):
        res.append(path)
        for i in range(index,len(nums)):
            if i != index and nums[i] == nums[i-1]:
                continue
            # path.append(nums[i])
            path = path + [nums[i]]
            self.dfs(res,path,nums,i+1)
            # path.pop()
            path = path[:len(path)-1]

#WEIRD

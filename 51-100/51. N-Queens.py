#Hard
#Backtracking
#Time Complexity: O(N^N)
#Space Complexity: O(N)

class Solution:
    def solveNQueens(self, n: int) -> List[List[str]]:
        res = []
        queens = [0]*n
        self.helper(res,queens,0)
        return res
    
    def helper(self,res,queens,pos):
        if pos == len(queens):
            self.addSolution(res,queens)
            return
        for i in range(len(queens)):
            queens[pos] = i
            if self.isValid(queens,pos):
                self.helper(res,queens,pos+1)
                
    def isValid(self,queens,pos):
        for i in range(pos):
            if queens[i] == queens[pos]:
                return False
            if abs(queens[i]-queens[pos])==abs(i-pos):
                return False
        return True
    
    def addSolution(self,res,queens):
        List = []
        for i in range(len(queens)):
            st = ""
            for j in range(len(queens)):
                if queens[i] == j:
                    st += "Q"
                else:
                    st += "."
            List.append(st)
        res.append(List)


#Easy
#Time Complexity: O(N)
#Space Complexity: O(1)
#Idea: go throught the list and get the length of the list, and link tail to head. and then go to the point that wants to break, and head goes to the point->next, and then break.

def rotateRight(self, head: Optional[ListNode], k: int) -> Optional[ListNode]:
        if k == 0 or not head:
            return head
        length = 1
        p = head
        while index.next:
            p = p.next
            length += 1
        k = length - k%length
        p.next = head
        for i in range(k):
            p = p.next
        head = p.next
        p.next = None
        return head

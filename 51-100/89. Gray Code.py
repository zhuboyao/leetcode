#Medium
#not very important

#G(i) = i^(i/2)

#Time: O(1<<n)
#Space: O(1<<n)
class Solution:
    def grayCode(self, n: int) -> List[int]:
        res = []
        for i in range(0,1<<n): #1<<n means 1 left-shift n bits
            res.append(i^i>>1) #i>>1 means i//2
        return res

#Medium
#The easiest one in backtracking

#Cascading
#Time complexity: O(N*2^N) to generate all subsets and then copy them into output list.
#Space complexity: O(N*2^N) This is exactly the number of solutions for subsets multiplied by the number NN of elements to keep for each subset.

class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        n = len(nums)
        output = [[]]
        for num in nums:
            output += [curr + [num] for curr in output]
        return output

#backtracking
class Solution:
    def subsets(self, nums: List[int]) -> List[List[int]]:
        res = []
        if not nums or len(nums)==0:
            return res
        self.helper(res,[],nums,0)
        return res
    
    def helper(self,res,list1,nums,index):
        res.append(list1[:])
        for i in range(index,len(nums)):
            list1.append(nums[i])
            self.helper(res,list1,nums,i+1)
            list1.pop()

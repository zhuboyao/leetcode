#Medium
#Time Complexity O(N)
#Space Complexity O(1)


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def reverseBetween(self, head: Optional[ListNode], left: int, right: int) -> Optional[ListNode]:
        dummy = ListNode(0)
        dummy.next = head
        pre = dummy
        cur = dummy.next
        for i in range(1,left):
            cur = cur.next
            pre = pre.next
        for i in range(left,right):
            temp = cur.next
            cur.next = temp.next
            temp.next = pre.next
            pre.next = temp
        return dummy.next

#pre always points to 1, cur always points to left, temp always points to cur->next..

#          l        r
#dummy->1->2->3->4->5->6
# pre  cur
#
#          l        r
#dummy->1->2->3->4->5->6
#       p  c  t
#
#          l        r
#dummy->1->3->2->4->5->6
#       p     c  t
#
#          l        r
#dummy->1->4->3->2->5->6
#       p     c  t
#
#          l        r
#dummy->1->5->4->3->2->6
#       p        c  t

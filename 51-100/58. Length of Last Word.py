#Easy
#Time: O(N)
#Space: O(1)

class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        p, length = len(s)-1,0
        while p > -1:
            if s[p] != ' ':
                length += 1
            elif length > 0:
                return length
            p -= 1
        return length

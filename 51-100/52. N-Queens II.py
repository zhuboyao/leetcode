#Hard
#Similar to 52, where res is used as list with one element to record the number

class Solution:
    def totalNQueens(self, n: int) -> int:
        queens = [0]*n
        res = [0]
        self.helper(res,queens,0)
        return res[0]
    
    def helper(self,res,queens,pos):
        if pos == len(queens):
            res.append(res.pop()+1)
            return
        for i in range(len(queens)):
            queens[pos] = i
            if self.isValid(queens,pos):
                self.helper(res,queens,pos+1)
                
    def isValid(self,queens,pos):
        for i in range(pos):
            if queens[i] == queens[pos]:
                return False
            if abs(queens[i]-queens[pos])==abs(i-pos):
                return False
        return True
    
    

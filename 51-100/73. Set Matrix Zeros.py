#Medium
#Time: O(M*N)
#Space: O(1)
#Idea: using first col and row to record if the corresponding row and col need to be set 0, and using row and col to record if the first row and col need to be set 0

class Solution:
    def setZeroes(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        if len(matrix) == 0:
            return
        m = len(matrix)
        n = len(matrix[0])
        row = False
        col = False
        for i in range(m):
            for j in range(n):
                if matrix[i][j] == 0:
                    matrix[i][0] = 0
                    matrix[0][j] = 0
                    if i == 0:
                        row = True
                    if j == 0:
                        col = True
        for i in range(1,m):
            if matrix[i][0] == 0:
                for j in range(1,n):
                    matrix[i][j] = 0
        for j in range(1,n):
            if matrix[0][j] == 0:
                for i in range(1,m):
                    matrix[i][j] = 0
        if row == True:
            for j in range(n):
                matrix[0][j] = 0
        if col == True:
            for i in range(m):
                matrix[i][0] = 0
        return

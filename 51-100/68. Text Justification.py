#Hard
#Not Often but important, GOOGLE
#Time: O(N)
#Space: O(N)

class Solution:
    def fullJustify(self, words: List[str], maxWidth: int) -> List[str]:
        res = []
        index = 0  #position at words
        while index < len(words):
            count = len(words[index])
            last = index + 1
            while last < len(words):
                if len(words[last]) + count + 1 > maxWidth:
                    break
                count += len(words[last]) + 1
                last += 1
            line = ""
            line += words[index]
            diff = last-index-1
            if last == len(words) or diff == 0:  # diff=0 means there is only one word in the line
                for i in range(index+1,last):
                    line += " "
                    line += words[i]
                for i in range(len(line),maxWidth):
                    line += " "
            else:
                spaces = (maxWidth - count)//diff
                r = (maxWidth-count)%diff  #7 space, but two locs, the first space will be filled one more space
                for i in range(index+1,last):
                    for k in range(spaces):
                        line += " "
                    if r > 0:
                        line += " "
                        r -= 1
                    line += " "
                    line += words[i]
            res.append(line)
            index = last
        return res
                        
                



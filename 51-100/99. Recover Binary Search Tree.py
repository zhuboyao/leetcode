#Medium

#Time Complexity: O(N)
#Space Complexity: O(N)


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def recoverTree(self, root: Optional[TreeNode]) -> None:
        """
        Do not return anything, modify root in-place instead.
        """
        nums = self.inorder(root)
        x,y = self.find_two_swapped(nums)
        self.recover(root,2,x,y)
    
    def inorder(self,root):
        if root:
            return self.inorder(root.left) + [root.val] + self.inorder(root.right)
        return []
    
    def find_two_swapped(self,nums):
        n = len(nums)
        x = y = None
        for i in range(n-1):
            if nums[i] > nums[i+1]:
                y = nums[i+1]
                if x == None:
                    x = nums[i]
                else:
                    break
        return x,y
        
    def recover(self,r,count,x,y):
        if r:
            if r.val == x or r.val == y:
                if r.val == x:
                    r.val = y
                else:
                    r.val = x
                count -= 1
                if count == 0:
                    return
            self.recover(r.left,count,x,y)
            self.recover(r.right,count,x,y)
    

#Medium
#Google
#Time: O(N)
#Space: O(1)

class Solution:
    def canJump(self, nums: List[int]) -> bool:
        max_step = 0
        for i in range(len(nums)):
            if i>max_step:
                return False
            max_step = max(nums[i]+i,max_step)
        return True

# refer to 45.  

#Medium
#Memorize, important
#Backtracking, dfs

#Time: k*C(n,k)
#Space: k*C(n,k)

class Solution:
    def combine(self, n: int, k: int) -> List[List[int]]:
        res = []
        self.helper(res, [], n, k, 1)
        return res
    
    def helper(self,res,list1,n,k,start):
        if k == 0:
            res.append(list1[:])
            return
        for i in range(start,n+1):
            list1.append(i)
            self.helper(res,list1,n,k-1,i+1)
            list1.pop()



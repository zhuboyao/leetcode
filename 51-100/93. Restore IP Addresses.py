#Medium
#Backtracking
#not that important
#Time Complexity: (3^4)
#Space Complexity: O(N)

class Solution:
    def restoreIpAddresses(self, s: str) -> List[str]:
        res = []
        res = self.helper(res,s,0,"",0)
        return res
    
    def helper(self, res, s, index, ret, count):
        if count > 4:
            return
        if count == 4 and index == len(s):
            res.append(ret)
            return
        for i in range(1,4):
            if index+1 > len(s):
                break
            temp = s[index:index+i]
            if (temp.startswith("0") and len(temp)>1) or (i==3 and int(temp)>255):
                continue
#                break is better
            self.helper(res,s,index+i,ret+temp if count==3 else ret+temp+".",count+1)
        return res
                

#Medium
#dfs
#Time: O(N*3^L) N: the number of cells, L: the length of the word
#Space: O(L)

class Solution:
    def exist(self, board: List[List[str]], word: str) -> bool:
        m = len(board)
        n = len(board[0])
        for i in range(m):
            for j in range(n):
                if self.dfs(board,i,j,0,word):
                    return True
        return False
    
    def dfs(self,board,i,j,index,word):
        if index == len(word):
            return True
        if i<0 or j<0 or i>=len(board) or j>=len(board[0]) or board[i][j]!=word[index]:
            return False
        c = board[i][j]
        board[i][j] = '#'
        res = self.dfs(board,i+1,j,index+1,word) or self.dfs(board,i-1,j,index+1,word) or self.dfs(board,i,j+1,index+1,word) or self.dfs(board,i,j-1,index+1,word)
        board[i][j] = c
        return res


#Medium
#Very Very Very important, especially in facebook using DP

#Use an array for DP to store the results, dp[i] is to store the number of decode ways for substring of s from index 0 to index i-1
#dp[i] can get the baton from two other previous indices, either i-1 or i-2. Two previous indices are involve since both single and two digits decodes are possible.
#Time: O(N)
#Space: O(N)
class Solution:
    def numDecodings(self, s: str) -> int:
        if len(s) == 0 or s[0] == "0":
            return 0
        length = len(s)
        dp = [0] * (length+1)
        dp[0] = 1
        dp[1] = 1
        for i in range(2,length+1):
            first = int(s[i-1:i])
            second = int(s[i-2:i])
            if first >= 1 and first <= 9:
                dp[i] += dp[i-1]
            if second >= 10 and second <= 26:
                dp[i] += dp[i-2]
        return dp[length]

#Time: O(N)
#Space: O(1)
class Solution:
    def numDecodings(self, s: str) -> int:
        if s[0] == "0":
            return 0
        two_back = 1
        one_back = 1
        for i in range(1,len(s)):
            current = 0
            if s[i] != '0':
                current = one_back
            two_digit = int(s[i-1:i+1])
            if two_digit >= 10 and two_digit <= 26:
                current += two_back
            two_back = one_back
            one_back = current
        return one_back

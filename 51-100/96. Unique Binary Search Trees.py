#Medium

#n = 3
#root: 1  left:0 right:2 f(0)*f(2)
#root: 2  left:1 right:3 f(1)*f(3)
#root: 3  left:2 right:0 f(2)*f(0)
#take sum


#Time: O(N^2)
#Space: O(N)
class Solution:
    def numTrees(self, n: int) -> int:
        res = [0]*(n+1)
        res[0] = 1
        for i in range(1,n+1):
            for j in range(0,i):
                res[i] += res[j]*res[i-j-1]
        return res[n]

#G(n) the number of unique BST for a sequence of length n.
#F(i,n) the number of unique BST, where i is served as root
#G(n) = \sum_i^n F(i,n), G(0) = 1, G(1) = 1
#
#Take [1,2,3,4,5,6,7] as an example, F(3,7) = G(2) * G(4)
#F(i,n) = G(i-1)*G(n-i)
#THus:::
#G(n) = \sum_i^n G(i-1)*G(n-i)
#

#Medium
#Time: O(M*N)
#Space: O(1)
#solve it in place to save memory.
#Assign 1 initially along the first row and col if it is 0, if it is 1, assign 0 to it and the rest of value. Then go through all the other elements, if it is 0, propagate using dynamic programming. if it is 1, change it to be 0.
class Solution:
    def uniquePathsWithObstacles(self, obstacleGrid: List[List[int]]) -> int:
        m = len(obstacleGrid)
        n = len(obstacleGrid[0])
        if obstacleGrid[0][0] == 1:
            return 0
        obstacleGrid[0][0] = 1
        for i in range(1,m):
            obstacleGrid[i][0] = int(obstacleGrid[i][0]==0 and obstacleGrid[i-1][0]==1)
        for j in range(1,n):
            obstacleGrid[0][j] = int(obstacleGrid[0][j]==0 and obstacleGrid[0][j-1]==1)
        for i in range(1,m):
            for j in range(1,n):
                if obstacleGrid[i][j] == 0:
                    obstacleGrid[i][j] = obstacleGrid[i-1][j] + obstacleGrid[i][j-1]
                else:
                    obstacleGrid[i][j] = 0
        return obstacleGrid[m-1][n-1]
        

#Time: O(M*N)
#Space: O(N)
class Solution:
    def uniquePathsWithObstacles(self, obstacleGrid: List[List[int]]) -> int:
        n = len(obstacleGrid[0])
        m = len(obstacleGrid)
        res = [0]*n
        res[0] = 1
        for i in range(m):
            for j in range(n):
                if obstacleGrid[i][j] == 1:
                    res[j] = 0
                elif j > 0:
                    res[j] += res[j-1]
        return res[n-1]

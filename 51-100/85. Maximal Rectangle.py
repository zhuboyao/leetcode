#Hard

#DP or Stack
#VERY IMPORTANT, better to master both methods


#DP
#Time complexity : O(NM). In each iteration over N we iterate over M a constant number of times.
#Space complexity : O(M). M is the length of the additional arrays we keep.
class Solution:
    def maximalRectangle(self, matrix: List[List[str]]) -> int:
        if not matrix:
            return 0
        m = len(matrix)
        n = len(matrix[0])
        left = [0]*n
        right = [n]*n
        height = [0]*n
        maxarea = 0
        
        for i in range(m):
            cur_left, cur_right = 0,n
            for j in range(n): #update height
                if matrix[i][j] == '1':
                    height[j] += 1
                else:
                    height[j] = 0
            for j in range(n): #update left
                if matrix[i][j] == '1':
                    left[j] = max(left[j],cur_left)
                else:
                    left[j] = 0
                    cur_left = j+1
            for j in range(n-1,-1,-1): #update right
                if matrix[i][j] == '1':
                    right[j] = min(right[j],cur_right)
                else:
                    right[j] = n
                    cur_right = j
            for j in range(n): #update area
                maxarea = max(maxarea, height[j]*(right[j]-left[j]))
        return maxarea
            
#Using Stack, similar to 84
#Time complexity : O(NM). In each iteration over N we iterate over M a constant number of times.
#Space complexity : O(M). M is the length of the additional arrays we keep.
class Solution:
    def maximalRectangle(self, matrix: List[List[str]]) -> int:
        if not matrix:
            return 0
        maxarea = 0
        dp = [0]*len(matrix[0])
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if matrix[i][j] == "1":
                    dp[j] = dp[j]+1
                else:
                    dp[j] = 0
            maxarea = max(maxarea, self.leetcode84(dp))
        return maxarea
        
        
    def leetcode84(self,heights):
        stack = [-1]
        maxarea = 0
        for i in range(len(heights)):
            while stack[-1] != -1 and heights[stack[-1]] >= heights[i]:
                maxarea = max(maxarea, heights[stack.pop()]*(i-stack[-1]-1))
            stack.append(i)
        while stack[-1] != -1:
            maxarea = max(maxarea, heights[stack.pop()]*(len(heights)-stack[-1]-1))
        return maxarea
        
        





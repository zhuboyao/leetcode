#Medium
#Time: O(log(MN))
#Space: O(1)
#Often

class Solution:
    def searchMatrix(self, matrix: List[List[int]], target: int) -> bool:
        m = len(matrix)
        if m == 0:
            return False
        n = len(matrix[0])
        begin, end = 0, m*n-1
        while begin <= end:
            mid = (end+begin)//2
            value = matrix[mid//n][mid%n]
            if value == target:
                return True
            elif value < target:
                begin = mid+1
            else:
                end = mid-1
        return False
            

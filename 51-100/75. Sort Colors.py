#Medium
#Often, may vary,
#Idea: use 3 pointers to track the rightmost boundary of 0, the leftmost boundary of 2 and the current element under the consideration

#0 0 2 1 1 2
#- - L   R -

#Time Complexity: O(n)
#Space Complexity: O(1)

class Solution:
    def sortColors(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        left = 0
        right = len(nums)-1
        index = 0
        while index<=right:
            if nums[index] == 0:
                nums[index], nums[left] = nums[left], nums[index]
                left += 1
                index += 1
            elif nums[index] == 1:
                index += 1
            else:
                nums[index], nums[right] = nums[right], nums[index]
                right -= 1
        return

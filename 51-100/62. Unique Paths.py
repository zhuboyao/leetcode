#Medium
#Time complexity: O(N×M)
#Space complexity:O(N×M)

class Solution:
    def uniquePaths(self, m: int, n: int) -> int:
        res = [[1 for _ in range(n)] for _ in range(m)]
        for i in range(1,m):
            for j in range(1,n):
                res[i][j] = res[i-1][j] + res[i][j-1]
        return res[m-1][n-1]

#Time complexity: O(N×M)
#Space complexity:O(N)
#Idea the same as above, but shrink the space
class Solution:
    def uniquePaths(self, m: int, n: int) -> int:
        res = [0]*n
        res[0] = 1
        for i in range(m):
            for j in range(1,n):
                res[j] = res[j]+res[j-1]
        return res[n-1]


#Time complexity: O(M)
#Space complexity:O(1)
#Combinatorial
class Solution:
    def uniquePaths(self, m: int, n: int) -> int:
        counts = m+n-2
        k = m-1
        res = 1
        for i in range(1,k+1):
            res = res*(counts-k+i)//i
        return int(res)

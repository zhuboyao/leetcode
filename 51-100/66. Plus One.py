#Easy
#Time Complexity O(N)
#Space Complexity O(1)

class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:
        carry = 1
        for i in range(len(digits)-1,-1,-1):
            tmp = digits[i] + carry
            carry = tmp//10
            digits[i] = tmp%10
            if carry == 0:
                return digits
        return [1]+digits

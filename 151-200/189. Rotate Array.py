#Medium

#Time Complexity: O(N)
#Space Complexity: O(1)
#Idea: reversing all numbers, reversing first k numbers, reversing last n-k numbers
class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        n = len(nums)
        k = k%n
        self.reverse(nums,0,n-1)
        self.reverse(nums,0,k-1)
        self.reverse(nums,k,n-1)
        
    def reverse(self,nums,start,end):
        while start<end:
            nums[start],nums[end] = nums[end],nums[start]
            start += 1
            end -= 1



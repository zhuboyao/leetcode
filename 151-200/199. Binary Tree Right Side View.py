#Medium


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
#DFS
#Time Complexity: O(N)
#Space Complexity: O(H), H the tree height
class Solution:
    def rightSideView(self, root: Optional[TreeNode]) -> List[int]:
        if not root:
            return []
        rightside = []
        self.helper(root,rightside,0)
        return rightside
    
    def helper(self,node,rightside,level):
        if not node:
            return res
        if level == len(rightside):
            rightside.append(node.val)
        for child in [node.right,node.left]:
            if child:
                self.helper(child,rightside,level+1)
                
        

#BFS
#Time Complexity: O(N)
#Space Complexity: O(D), Tree Diameter
class Solution:
    def rightSideView(self, root: Optional[TreeNode]) -> List[int]:
        if not root:
            return []
        queue = deque([root])
        rightside = []
        while queue:
            level_length = len(queue)
            for i in range(level_length):
                node = queue.popleft()
                if i == level_length-1:
                    rightside.append(node.val)
                if node.left:
                    queue.append(node.left)
                if node.right:
                    queue.append(node.right)
        return rightside
                    

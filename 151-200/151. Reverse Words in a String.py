#Medium

#Time: O(N)
#Space: O(N)
class Solution:
    def reverseWords(self, s: str) -> str:
        return " ".join(reversed(s.split()))



class Solution:
    def reverseWords(self, s: str) -> str:
        stringBuilder = deque()
        s = s.strip()
        words = s.split()
        for word in words:
            stringBuilder.appendleft(word)
        return " ".join(stringBuilder)

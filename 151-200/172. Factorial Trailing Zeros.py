#Medium
#
#Time Complexity: O(logN)
#Space Complexity: O(1)
class Solution:
    def trailingZeroes(self, n: int) -> int:
        zero = 0
        for i in range(5,n+1,5):
            current = i
            while current % 5 == 0:
                zero += 1
                current //= 5
        return zero

#Time: O(logN)
#Space: O(N)
class Solution:
    def trailingZeroes(self, n: int) -> int:
        if n == 0:
            return 0
        return n//5 + self.trailingZeroes(n//5)

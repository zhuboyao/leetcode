#Hard

#Time Complexity: O(N)
#Space Complexity: O(b) bucket space

class Solution:
    def maximumGap(self, nums: List[int]) -> int:
        if len(nums) < 2:
            return 0
        length = len(nums)
        maxval = max(nums)
        minval = min(nums)
        maxint = sys.maxsize
        minint = -sys.maxsize-1
        
        gap = int(ceil(float(maxval-minval)/float(length-1))) #min gap
        bucketsMax = [minint]*(length-1)
        bucketsMin = [maxint]*(length-1)
        
        for num in nums:
            if num == minval or num == maxval:
                continue
            bucket = (num-minval)//gap
            bucketsMin[bucket] = min(num, bucketsMin[bucket])
            bucketsMax[bucket] = max(num, bucketsMax[bucket])
            
        res = 0
        pre = minval
        for i in range(length-1):
            if bucketsMin[i] == maxint and bucketsMax[i] == minint:
                continue
            res = max(res, bucketsMin[i]-pre)
            pre = bucketsMax[i]
        res = max(res,maxval-pre)
        return res
            
        
            

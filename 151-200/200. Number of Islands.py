#Medium

#DFS
#Time Complexity: O(M*N)
#Space Complexity: O(M*N) in case that the grid map is filled with lands where DFS goes by M*N deep
class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        if len(grid) == 0 or len(grid[0]) == 0:
            return 0
        nr = len(grid)
        nc = len(grid[0])
        num_islands = 0
        for r in range(nr):
            for c in range(nc):
                if grid[r][c] == "1":
                    num_islands += 1
                    self.dfs(grid,r,c)
        return num_islands
    
    def dfs(self,grid,r,c,):
        nr = len(grid)
        nc = len(grid[0])
        if r<0 or c<0 or r>=nr or c>=nc or grid[r][c]=="0":
            return
        grid[r][c] = '0'
        self.dfs(grid,r-1,c)
        self.dfs(grid,r+1,c)
        self.dfs(grid,r,c-1)
        self.dfs(grid,r,c+1)
        

#BFS
#Time: O(M*N)
#Space: O(min(M,N))
class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        if len(grid) == 0 or len(grid[0]) == 0:
            return 0
        nr = len(grid)
        nc = len(grid[0])
        num_islands = 0
        
        for r in range(nr):
            for c in range(nc):
                if grid[r][c] == '1':
                    num_islands += 1
                    grid[r][c] = '0' #mark as visited
                    neighbors = deque()
                    neighbors.append(r*nc+c)
                    while len(neighbors):
                        index = neighbors.pop()
                        row = index//nc
                        col = index%nc
                        if row-1>=0 and grid[row-1][col] == "1":
                            neighbors.append((row-1)*nc+col)
                            grid[row-1][col] = '0'
                        if row+1<nr and grid[row+1][col] == "1":
                            neighbors.append((row+1)*nc+col)
                            grid[row+1][col] = '0'
                        if col-1>=0 and grid[row][col-1] == "1":
                            neighbors.append(row*nc+col-1)
                            grid[row][col-1] = '0'
                        if col+1<nc and grid[row][col+1] == "1":
                            neighbors.append(row*nc+col+1)
                            grid[row][col+1] = '0'
        return num_islands

#Easy

#Time Complexity: O(1)
#Space Complexity: O(1)
class Solution:
    def reverseBits(self, n: int) -> int:
        if n==0:
            return 0
        res = 0
        for _ in range(32):
            res = res<<1
            if n&1 == 1:
                res += 1
            n = n>>1
        return res

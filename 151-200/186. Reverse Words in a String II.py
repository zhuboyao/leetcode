#Medium

#Time: O(N)
#Space: O(1)

class Solution:
    def reverseWords(self, s: List[str]) -> None:
        """
        Do not return anything, modify s in-place instead.
        """
        # reverse the whole string, and reverse each word
        self.reverse(s,0,len(s)-1)
        self.reverse_each_word(s)
        
    def reverse(self,s,start,end):
        while start < end:
            s[start], s[end] = s[end], s[start]
            start += 1
            end -= 1
    
    def reverse_each_word(self,s):
        n = len(s)
        l1 = l2 = 0
        while l1<n:
            while l2<n and s[l2] != ' ':
                l2 += 1
            self.reverse(s,l1,l2-1)
            l1 = l2+1
            l2 += 1



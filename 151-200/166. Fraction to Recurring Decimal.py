#Medium

class Solution:
    def fractionToDecimal(self, numerator: int, denominator: int) -> str:
        # +，-; interger; decimal
        if numerator == 0:
            return '0'
        res = ""
        res += "-" if numerator*denominator<0 else ""
        num = abs(numerator)
        den = abs(denominator)
        
        res += str(num//den)
        num = num%den
        
        if num == 0:
            return res
        res += "."
        maps = {}
        maps[num] = len(res)
        while num!=0:
            num *= 10
            res += str(num//den)
            num = num%den
            if num in maps:
                index = maps[num]
                res = res[:index] + "(" + res[index:] + ")"
                break
            else:
                maps[num] = len(res)
        return res

#Easy

#Time Complexity: O(N)
#Space Complexity: O(1)

class Solution:
    def findMissingRanges(self, nums: List[int], lower: int, upper: int) -> List[str]:
        res = []
        alower = lower
        aupper = upper
        for num in nums:
            if num == alower:
                alower += 1
            elif alower < num:
                if alower+1 == num:
                    res.append(str(alower))
                else:
                    res.append(str(alower)+"->"+str(num-1))
                alower = num+1
        if alower == aupper: #boundary
            res.append(str(alower))
        elif alower < aupper:
            res.append(str(alower)+"->"+str(aupper))
        return res


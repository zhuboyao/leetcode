#Medium

#Time Complexity: O(N)
#Space Complexity: O(1)
#Idea: rob(i) = max(rob(i+1),rob(i+2)+nums[i])
#DP

class Solution:
    def rob(self, nums: List[int]) -> int:
        if not nums:
            return 0
        if len(nums) == 1:
            return nums[0]
        
        t1 = t2 = 0
        for current in nums:
            temp = t1
            t1 = max(t1,t2+current)
            t2 = temp
        return t1

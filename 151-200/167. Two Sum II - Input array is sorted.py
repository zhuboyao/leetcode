#Easy

# already sorted, not need map,
#Time Complexity: O(N)
#Space Complexity: O(1)

class Solution:
    def twoSum(self, numbers: List[int], target: int) -> List[int]:
        low, high = 0, len(numbers)-1
        while low < high:
            S = numbers[low] + numbers[high]
            if S == target:
                return [low+1,high+1]
            elif S < target:
                low += 1
            else:
                high -= 1
        return [-1,-1]

#Easy

#Time: O(NlogN)
#Space: O(1)
class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        nums.sort()
        return nums[len(nums)//2]

#Using HashTable, now shown here


#Boyer-Moore Voting Algorithm
#Time: O(N)
#Space: O(1)
class Solution:
    def majorityElement(self, nums: List[int]) -> int:
        count = 0
        candidate = None
        for num in nums:
            if count == 0:
                candidate = num
            if num != candidate:
                count -= 1
            else:
                count += 1
        return candidate

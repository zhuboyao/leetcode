#Medium

#Time Complexity: O(NlogN)
#Space Complexity: O(N)
class Solution:
    def largestNumber(self, nums: List[int]) -> str:
        largest_num = "".join(sorted(map(str,nums),key=LargerNumKey))
        if largest_num[0] == '0':
            return '0'
        return largest_num
        
class LargerNumKey(str):
    def __lt__(x,y):
        return x+y>y+x


#Medium

#Similar to 33

#Time Complexity: O(logN)
#Space Complexity: O(1)
class Solution:
    def findMin(self, nums: List[int]) -> int:
        if not nums:
            return -1
        start, end = 0, len(nums)-1
        while start + 1 < end:
            mid = (end+start)//2
            if nums[mid] < nums[end]:
                end = mid
            else:
                start = mid+1
        if nums[start] < nums[end]:
            return nums[start]
        return nums[end]

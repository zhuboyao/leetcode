#Easy

#Time Complexity: ?? O(N)
#Space Complexity: ?? O(N)

class Solution:
    def findRepeatedDnaSequences(self, s: str) -> List[str]:
        seen = set([])
        repeated = set([])
        for i in range(len(s)-9):
            temp = s[i:i+10]
            if temp in seen:
                repeated.add(temp)
            seen.add(temp)
        return list(repeated)

#Easy
#Very Often, use stack
#Time: O(n)
#Sapce: O(n)

class Solution:
    def isValid(self, s: str) -> bool:
        if s is None or len(s) == 0:
            return True
        stack = []
        for i in s:
            if i == '(':
                stack.append(')')
            elif i == '[':
                stack.append(']')
            elif i == '{':
                stack.append('}')
            elif not stack or i != stack.pop():
                return False
        return len(stack) == 0

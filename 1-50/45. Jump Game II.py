#Medium
#Time Complexity: O(N) because there are N elements in the array and we visit each element in the array only once.
#Space Complexity: O(1) because we don't use any additional data structures.

class Solution:
    def jump(self, nums: List[int]) -> int:
        jumps = 0
        current_jump_end = 0
        farthest = 0
        for i in range(len(nums)-1):
            farthest = max(farthest, i+nums[i])
            if i == current_jump_end:
                jumps+=1
                current_jump_end = farthest
        return jumps


# "farthest" records farthest location that next jump can go, while "current_jump_end" records current jump can go, while i==current_jump_end, we calculate the next.

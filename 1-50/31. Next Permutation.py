#Medium
#Very Important
#Time complexity : O(n)O(n). In worst case, only two scans of the whole array are needed.
#Space complexity : O(1)O(1). No extra space is used. In place replacements are done.

class Solution:
    def nextPermutation(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        n = len(nums)
        i = n-1
        while i>0 and nums[i] <= nums[i-1]:
            i -= 1
        self.reverse(nums, i, n-1)
        if i > 0:
            for j in range(i,n):
                if nums[j] > nums[i-1]:
                    self.swap(nums,i-1,j)
                    break
    def reverse(self, nums, i, j):
        while i < j:
            self.swap(nums,i,j)
            i += 1
            j -= 1
    def swap(self,nums,i,j):
        nums[i],nums[j] = nums[j],nums[i]
        
# Idea: example like nums = [1,2,7,4,3,1]
# searching from the end until nums[i-1] < nums[i], i.e. 2 < 7
# i->7 right now, reverse(nums, i, n-1)--->[1,2,1,3,4,7]  if i > 0, swaps nums[i-1](2) to the first element on the right hand side that is larger than nums[i-1](3)--->[1,3,1,2,4,7]

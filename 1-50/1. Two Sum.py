#Easy
#Time Complexity O(n)
#Space Complexity O(n)
# 167 Two Sum II
# 15 3 Sum
# 16 3 Sum Closet
# 259 3 Sum Smaller
# 18 4 Sum

class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        lookup = dict()
        for i in range(len(nums)):
            if target-nums[i] in lookup.keys():
                return [i,lookup[target-nums[i]]]
            lookup[nums[i]] = i


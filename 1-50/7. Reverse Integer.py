#Medium
#Time Complexity: O(log(x)); log(x)=log_10(x)
#Space Complexity: O(1)

class Solution:
    def reverse(self, x: int) -> int:
        sign = -1 if x < 0 else 1
        res = 0
        x *= sign
        while x!=0:
            res = (res*10)+(x%10)
            x = x//10
        if res >2**31-1:
            return 0
        return res*sign

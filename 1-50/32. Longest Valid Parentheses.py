#Hard
#Time complexity: O(n). nn is the length of the given string.
#Space complexity: O(n). The size of stack can go up to nn.


#Using Stack
class Solution:
    def longestValidParentheses(self, s: str) -> int:
        maxans = 0
        stack = [-1]
        for i in range(len(s)):
            if s[i] == "(":
                stack.append(i)
            else:
                stack.pop()
                if len(stack) == 0:
                    stack.append(i)
                else:
                    maxans = max(maxans, i-stack[-1])
        return maxans



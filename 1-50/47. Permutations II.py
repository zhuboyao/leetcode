#Medium
#Complexity

class Solution:
    def __init__(self):
        self.count = 0
    def permuteUnique(self, nums: List[int]) -> List[List[int]]:
        if len(nums) <= 1:
            return [nums]
        res = []
        if self.count == 0:
            nums.sort()
            self.count += 1
        for i,num in enumerate(nums):
            if i>0 and nums[i-1] == nums[i]:
                continue
            nums2 = nums[:i] + nums[i+1:]
            for y in self.permuteUnique(nums2):
                res.append([num]+y)
        return res


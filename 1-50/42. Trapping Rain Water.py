#Hard
#Time Complexity: O(n)
#Space Complexity: O(1)

class Solution:
    def trap(self, height: List[int]) -> int:
        n = len(height)
        if n<3: return 0
        l,r = 0,n-1
        maxL = height[l]
        maxR = height[r]
        ans = 0
        while l<r:
            if maxL < maxR:
                ans += maxL-height[l]
                l += 1
                maxL = max(maxL,height[l])
            else:
                ans += maxR-height[r]
                r -= 1
                maxR = max(maxR,height[r])
        return ans

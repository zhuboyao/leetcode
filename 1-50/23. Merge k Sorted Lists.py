#Hard

#Time: O(Nlogk)
#Space: O(n)
#Compare one by one by Priority Queue
class Solution(object):
    def mergeKLists(self, lists):
        h = [(l.val,idx) for idx, l in enumerate(lists) if l]
        heapq.heapify(h)
        head = cur = ListNode(None)
        while h:
            val, idx = heapq.heappop(h)
            cur.next = ListNode(val)
            cur = cur.next
            node = lists[idx].next
            lists[idx] = lists[idx].next
            if node:
                heapq.heappush(h,(node.val,idx))
        return head.next


#Merge with Divide and Conquer
#Time: O(Nlogk)
#Space: O(1)

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def mergeKLists(self, lists: List[Optional[ListNode]]) -> Optional[ListNode]:
        amount = len(lists)
        interval = 1
        while interval < amount:
            for i in range(0,amount-interval,interval*2):
                lists[i] = self.merge2Lists(lists[i],lists[i+interval])
            interval *= 2
        return lists[0] if amount > 0 else None
        
        
    def merge2Lists(self,l1,l2):
        head = point = ListNode(0)
        while l1 and l2:
            if l1.val<=l2.val:
                point.next = l1
                l1 = l1.next
            else:
                point.next = l2
                l2 = l2.next
            point = point.next
        if not l1:
            point.next = l2
        else:
            point.next = l1
        return head.next

#Medium
#Facebook
#Time O(don't know)
#Space O(N)


class Solution:
    def countAndSay(self, n: int) -> str:
        i = 1
        res = "1"
        while i < n:
            count = 0
            stringBuilder = ""
            c = res[0]
            for j in range(len(res)+1):
                if j!=len(res) and res[j]==c:
                    count += 1
                else:
                    stringBuilder += str(count)
                    stringBuilder += c
                    if j != len(res):
                        count = 1
                        c = res[j]
            res = stringBuilder
            i += 1
        return res
        
class Solution:
    def countAndSay(self, n: int) -> str:
        seq = "1"
        for i in range(n-1):
            seq = self.getNext(seq)
        return seq
    def getNext(self, seq):
        i, next_seq = 0, ""
        while i < len(seq):
            count = 1
            while i < len(seq)-1 and seq[i] == seq[i+1]:
                count += 1
                i += 1
            next_seq += str(count) + seq[i]
            i += 1
        return next_seq


#Medium
#Time Complexity: O(M*N)
#Space Complexity: O(M+N)


class Solution:
    def multiply(self, num1: str, num2: str) -> str:
        if num1=="0" or num2=="0":
            return "0"
        digits = [0]*(len(num1)+len(num2))
        for i in range(len(num1)-1,-1,-1):
            for j in range(len(num2)-1,-1,-1):
                product = int(num1[i])*int(num2[j])
                p1 = i+j
                p2 = i+j+1
                Sum = product + digits[p2]
                digits[p1] += Sum//10
                digits[p2] = Sum%10
        digits = digits[::-1]
        if digits[-1]==0:
            digits.pop()
        
        return ''.join(str(digit) for digit in reversed(digits))

#Medium
#Important, good to remember

#Backtracking
#Time: O(\sum_k=1^N P(N,k))
#Space: O(N!)
#class Solution:
#    def permute(self, nums: List[int]) -> List[List[int]]:
#        res = []
#        if len(nums) == 0:
#            return res
#        self.helper(nums,res,[])
#        return res
#
#    def helper(self,nums,res,ls):
#        if len(ls) == len(nums):
#            res.append(list(ls))
#            return
#        for i in range(len(nums)):
#            if nums[i] in ls:
#                continue
#            ls.append(nums[i])
#            self.helper(nums,res,ls)
#            ls.pop()



class Solution:
    def permute(self, nums: List[int]) -> List[List[int]]:
        if len(nums) <= 1:
            return [nums]
        answer= []
        
        for i, num in enumerate(nums):
            n = nums[:i] + nums[i+1:]
            for y in self.permute(n):
                answer.append([num]+y)
        return answer

#Easy
#Time complextiy : O(n). Assume that n is the length of array. Each of i and j traverses at most n steps.
#Space complexity : O(1).

class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        if len(nums) == 0:
            return 0
        count = 1
        for i in range(1,len(nums)):
            if nums[i-1] != nums[i]:
                nums[count] = nums[i]
                count += 1
        return count

#Medium
#Time complexity : O(logN).
#Space complexity : O(1).

class Solution:
    def search(self, nums: List[int], target: int) -> int:
        start = 0
        end = len(nums)-1
        while start <= end:
            mid = (end-start)//2+start
            if nums[mid] == target:
                return mid
            if nums[start]<=nums[mid]:
                if nums[start] <= target and target < nums[mid]:
                    end = mid-1
                else:
                    start = mid+1
            if nums[mid]<=nums[end]:
                if nums[mid] < target and target <= nums[end]:
                    start = mid+1
                else:
                    end = mid-1
        return -1

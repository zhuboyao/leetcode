#Medium

#Dynamic Programming
#Time: O(n^2)
#Space: O(n^2)

class Solution:
    def longestPalindrome(self, s: str) -> str:
        if s=="": return ""
        res = s[0]
        lens = len(s)
        dp = [[False]*lens for _ in range(lens)]
        maxl = 0
        for j in range(lens):
            for i in range(j):
                dp[i][j] = (s[i]==s[j]) and (j-i<=2 or dp[i+1][j-1])
                if dp[i][j] == True:
                    if j-i+1>maxl:
                        maxl = j-i+1
                        res = s[i:j+1]
        return res
        
#Expand Around Center
#Time: O(n^2)
#Space: O(n)
class Solution:
    res = ""
    def longestPalindrome(self, s: str) -> str:
        if s == "":
            return ""
        for i in range(len(s)):
            self.helper(s,i,i)
            self.helper(s,i,i+1)
        return self.res
    
    def helper(self,s,left,right):
        while left>=0 and right<len(s) and s[left] == s[right]:
            left -= 1
            right += 1
        cur = s[left+1:right]
        if len(cur) > len(self.res):
            self.res = cur


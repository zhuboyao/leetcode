#Medium
#Time Complexity: O(logN)
#Space Complexity: O(1)

class Solution:
    def searchRange(self, nums: List[int], target: int) -> List[int]:
        if len(nums) == 0:
            return [-1,-1]
        low, high = 0, len(nums)-1
        while low<=high:
            if target < nums[low] or nums[high] < target:
                return [-1,-1]
            else:
                mid = (low+high)//2
                if nums[mid] < target:
                    low = mid + 1
                elif target < nums[mid]:
                    high = mid - 1
                else:
                    while nums[low] != target:
                        low += 1
                    while nums[high] != target:
                        high -= 1
                    return [low,high]
        return [-1,1]

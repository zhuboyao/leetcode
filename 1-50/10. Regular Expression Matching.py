#Hard
#Time Complexity: O(n*m)
#Space Complexity: O(n*m)
#Very Important Facebook

class Solution:
    def isMatch(self, s: str, p: str) -> bool:
        dp = [[False]*(len(p)+1) for _ in range(len(s)+1)]
        dp[0][0] = True
        for i in range(len(p)):
            if p[i] == '*' and dp[0][i-1]:
                dp[0][i+1] = True
        for i in range(len(s)):
            for j in range(len(p)):
                if p[j] == s[i]:
                    dp[i+1][j+1] = dp[i][j]
                if p[j] == '.':
                    dp[i+1][j+1] = dp[i][j]
                if p[j] == '*':
                    if p[j-1] != s[i] and p[j-1] != '.':
                        dp[i+1][j+1] = dp[i+1][j-1]
                    else:
                        dp[i+1][j+1] = dp[i+1][j] or dp[i][j+1] or dp[i+1][j-1]
        return dp[len(s)][len(p)]
                    

#a*b -> aab
#c* -> empty
#dp[i][j] means if s[i:] and p[j:] match

#dp[0][0] = True
#1. p[j] == s[i]: dp[i][j] = dp[i-1][j-1]
#2. if p[j] == '.':
#        dp[i][j] = dp[i][j] = dp[i-1][j-1]
#3. if p[j] == '*':
#    here are two sub conditions:
#    1. if p[j-1] != s[i]:
#            dp[i][j] = dp[i][j-2]  // aab , c*aab, delete c*
#    2. if p[j-1] == s[i] or p[j-1] == '.': //aa, a*; or aa, .*
#            dp[i][j] = dp[i][j-1]   // in this case, a* counts as single a
#            dp[i][j] = dp[i-1][j]   // in this case, a* counts as multiple a: aaa, a*
#            dp[i][j] = dp[i][j-2]   // in this case, a* counts as empty

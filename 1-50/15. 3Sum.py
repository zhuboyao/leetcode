#Medium
#very often, important, pre-requisite: 1 167
#Idea: sorting to eliminate duplicates, reduce dimension by using a for loop + 2Sum
#Time: O(n^2)
#Space: O(n)

#Followup, No sorting
#Similar problems: 1, 167, 16, 259, 18

class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        res = []
        nums = sorted(nums)
        for i in range(len(nums)-2):
            if i > 0 and nums[i] == nums[i-1]:
                continue
            low = i+1
            high = len(nums)-1
            Sum = 0-nums[i]
            while low<high:
                if nums[low]+nums[high] == Sum:
                    res.append([nums[i],nums[low],nums[high]])
                    while low < high and nums[low] == nums[low+1]: low+=1
                    while low < high and nums[high] == nums[high-1]: high-=1
                    low += 1
                    high -= 1
                elif nums[low]+nums[high]<Sum:
                    low += 1
                else:
                    high -= 1
        return res







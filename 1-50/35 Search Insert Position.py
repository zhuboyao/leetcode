#Easy
#Time complexity : O(logN).
#Space complexity : O(1)
#Similar:
#278 First Bad Version
#35 Search Insert Position
#33 Search in Rotated Sorted Array
#81 Search in Rotated Sorted Array II
#153 Find Minimum in Rotated Sorted Array
#154 Find Minimum in Rotated Sorted Array II
#162 Find Peak Element
#374 Guess Number higher or Lower

class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        left, right = 0, len(nums)-1
        while left <= right:
            mid = (right-left)//2+left
            if nums[mid] == target:
                return mid
            elif nums[mid]>target:
                right = mid-1
            else:
                left = mid+1
        return left

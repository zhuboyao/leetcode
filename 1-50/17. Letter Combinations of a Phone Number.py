#Medium

#Backtracking
#Time Complexity: O(4^N * N), N the length of digits, 4 is referring to the maximum value length in the hash map, the worst cases contain 7s and 9s only, we have to explore 4 additional paths for every extra digit.
#Space Complexity: O(N), the length of digits

class Solution:
    mapping = ["0","1","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"]
    
    def helper(self,res,digits,s,index):
        if index == len(digits):
            res.append(s)
            return
        letters = self.mapping[int(digits[index])]
        for i in range(len(letters)):
            self.helper(res,digits,s+letters[i],index+1)
    
    def letterCombinations(self, digits: str) -> List[str]:
        res = []
        if not digits or len(digits) == 0:
            return res
        self.helper(res,digits,"",0)
        return res

#Easy
#Time Complexity: O(log(x)); log(x) = log_10(x)
#Space Complexity: O(1)


class Solution:
    def isPalindrome(self, x: int) -> bool:
        if x<0 or (x!=0 and not x%10):
            return False
        palind = x
        rev = 0
        while x > 0:
            rev = rev*10+x%10
            x = x//10
        return palind == rev

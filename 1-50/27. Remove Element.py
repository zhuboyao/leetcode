#Easy
#Similar: 26, 80
#Time complexity : O(n). Assume the array has a total of n elements, both i and j traverse at most 2n steps.
#Space complexity : O(1).


class Solution:
    def removeElement(self, nums: List[int], val: int) -> int:
        if len(nums) == 0:
            return 0
        res = 0
        for i in range(len(nums)):
            if nums[i] != val:
                nums[res] = nums[i]
                res += 1
        return res

#Hard
#Time Complexity O(n)
#Space Complexity O(1)
class Solution:
    def firstMissingPositive(self, nums: List[int]) -> int:
        n = len(nums)
        for i in range(n):
            while nums[i]>0 and nums[i]<=len(nums) and nums[i]!=nums[nums[i]-1]:
                temp = nums[nums[i]-1]
                nums[nums[i]-1] = nums[i]
                nums[i] = temp
        for i in range(n):
            if nums[i] != i+1:
                return i+1
        return n+1


#nums[i]!=nums[nums[i]-1] from nums[i] = i+1

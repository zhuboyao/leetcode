#Medium
#Maybe rare but important
#Time: O(logn)
#Space: O(logn)

class Solution:
    def divide(self, dividend: int, divisor: int) -> int:
        sign = 1
        if (dividend > 0 and divisor < 0) or (dividend < 0 and divisor > 0):
            sign = -1
        ldividend = abs(dividend)
        ldivisor = abs(divisor)
        if ldividend < ldivisor or ldividend == 0:
            return 0
        lres = self.divideLong(ldividend, ldivisor)
        if lres>2**31-1:
            if sign == 1:
                return 2**31-1
            else:
                return -2**31
        else:
            return sign*lres
        
        
    def divideLong(self, ldividend, ldivisor):
        if ldividend < ldivisor:
            return 0
        sum_val = ldivisor
        multiple = 1
        while (sum_val + sum_val) <= ldividend:
            sum_val += sum_val
            multiple += multiple
        return multiple + self.divideLong(ldividend-sum_val,ldivisor)

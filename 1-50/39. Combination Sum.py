#Medium
#very often
#Backtracking dfs
#Let N be the number of candidates, T be the target value, and M be the minimal value among the candidates.
#Time Complexity: O(N^{T/M+1}),
#Space Complexity: O(T/M


class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> List[List[int]]:
        res = []
        # candidates.sort()
        self.dfs(candidates,target,0,res,[])
        return res
    def dfs(self,nums,target,index,res,path):
        if target<0:
            return
        elif target == 0:
            res.append(path)
            return
        for i in range(index,len(nums)):
            # if nums[index] > target:
            #     return
            self.dfs(nums,target-nums[i],i,res,path+[nums[i]])


#we can comment out the three lines above

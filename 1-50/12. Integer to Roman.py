#Medium
#Time Complexity: O(n)
#Space Complexity: O(n)

class Solution:
    def intToRoman(self, num: int) -> str:
        values = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
        strs = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]
        stringBuilder = ""
        for i in range(len(values)):
            while num >= values[i]:
                num -= values[i]
                stringBuilder += strs[i]
        return stringBuilder


#Time Complexity: O(1)
#Space Complexity: O(1)
class Solution:
    def intToRoman(self, num: int) -> str:
        digits = [(1000,'M'), (900,"CM"),(500,"D"),(400,"CD"),(100,"C"),(90,"XC"),(50,"L"),(40,"XL"),(10,"X"),(9,"IX"),(5,"V"),(4,"IV"),(1,"I")]
        roman_digits = []
        for value, symbol in digits:
            if num == 0:
                break
            count, num = divmod(num,value)
            roman_digits.append(symbol*count)
        return "".join(roman_digits)

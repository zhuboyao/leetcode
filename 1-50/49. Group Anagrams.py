#Medium
#Very foundamental, often
#Similar: 387, 383, 242

#Couting Sort
#Idea, for each word, create a list of size 26 with all inital values 0, counting the number of appearance of each character. convert the list(count) to tuple and as a key, and append corresponding strings.

#Time Complexity: O(NK), where N is the length of strs, and K is the maximum length of a string in strs. Counting each string is linear in the size of the string, and we count every string.
#Space Complexity: O(NK), the total information content stored in ans.

class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        hashTable = collections.defaultdict(list)
        for s in strs:
            count = [0]*26
            for c in s:
                count[ord(c)-ord('a')] += 1
            hashTable[tuple(count)].append(s)
        return hashTable.values()


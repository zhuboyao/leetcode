#Medium
#Time Complexity O(n)
#Space Complexity O(n)

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        dummy = ListNode(0)
        Sum = 0
        cur = dummy
        p1, p2 = l1, l2
        while p1 != None or p2 != None:
            if p1 != None:
                Sum += p1.val
                p1 = p1.next
            if p2 != None:
                Sum += p2.val
                p2 = p2.next
            cur.next = ListNode(Sum%10)
            Sum = Sum//10
            cur = cur.next
        if Sum == 1:
            cur.next = ListNode(1)
        return dummy.next

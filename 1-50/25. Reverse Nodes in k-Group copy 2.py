#Hard
#Trick, rarely apprear in an interview
#Time: O(n)
#Space: O(n)

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def reverseKGroup(self, head: Optional[ListNode], k: int) -> Optional[ListNode]:
        if head is None or head.next is None:
            return head
        count = 0
        cur = head
        while cur is not None and count != k:
            cur = cur.next
            count += 1
        if count == k:
            cur = self.reverseKGroup(cur,k)
            while count>0:
                count -= 1
                temp = head.next
                head.next = cur
                cur = head
                head = temp
            head = cur
        return head

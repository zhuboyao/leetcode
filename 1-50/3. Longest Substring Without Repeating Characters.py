#Medium
#Time Complexity O(n)
#Space Complexity O(n)

class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        if s == "" or len(s) == 0:
            return 0
        hashTable = {}
        res = 0
        i = 0
        for j in range(len(s)):
            if s[j] in hashTable:
                i = max(hashTable[s[j]],i)
            res = max(res, j-i+1)
            hashTable[s[j]] = j+1
        return res

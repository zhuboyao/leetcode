#Easy
#Time Complexity: O(m*n)
#Space Complexity: O(1)


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        if len(strs) == 0:
            return ""
        for i in range(len(strs[0])):
            c = strs[0][i]
            for j in range(1,len(strs)):
                if i >= len(strs[j]) or strs[j][i] != c:
                    return strs[0][:i]
        return strs[0] if strs else ""


#Idea: take strs[0] as a reference and go through it and compare each single character with other words at corresponding position.

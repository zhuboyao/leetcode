#Hard
#Time: (9!)^9 or 9^81 for brute force
#Space: 81

class Solution:
    def solveSudoku(self, board: List[List[str]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        for i in range(9):
            for j in range(9):
                if board[i][j]=='.':
                    for k in '123456789':
                        board[i][j]=k
                        if self.isValid(board,i,j) and self.solveSudoku(board):
                            return True
                        board[i][j]='.'
                    return False
        return True
        
    def isValid(self,board,x,y):
            tmp=board[x][y]; board[x][y]='D'
            for i in range(9):
                if board[i][y]==tmp: return False
            for i in range(9):
                if board[x][i]==tmp: return False
            for i in range(3):
                for j in range(3):
                    if board[(x//3)*3+i][(y//3)*3+j]==tmp: return False
            board[x][y]=tmp
            return True   

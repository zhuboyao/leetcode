#Easy
#Time Complexity: O(1)
#Space Complexity: O(1)

class Solution:
    values = {
        "I":1,
        "V":5,
        "X":10,
        "L":50,
        "C":100,
        "D":500,
        "M":1000,
    }
    def romanToInt(self, s: str) -> int:
        total = 0
        i = 0
        while i < len(s):
            if i+1 < len(s) and self.values[s[i]] < self.values[s[i+1]]:
                total += self.values[s[i+1]]-self.values[s[i]]
                i += 2
            else:
                total += self.values[s[i]]
                i += 1
        return total







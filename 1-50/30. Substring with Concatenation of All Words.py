#Hard
#Time: O(n^2)
#Space: O(n)


class Solution:
    def findSubstring(self, s: str, words: List[str]) -> List[int]:
        if len(s) == 0 or len(words) == 0:
            return []
        res = []
        n = len(words)
        m = len(words[0])
        mapset = collections.defaultdict(lambda:0)
        for st in words:
            mapset[st] += 1
        for i in range(len(s)-n*m+1):
            copyset = collections.defaultdict(lambda:0)
            copyset = copy.deepcopy(mapset)
            k = n
            j = i
            while k>0:
                st = s[j:j+m]
                if st not in copyset or copyset[st]<1:
                    break
                copyset[st] -= 1
                k -= 1
                j += m
            if k == 0:
                res.append(i)
        return res

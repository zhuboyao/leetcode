#Medium
#Time Complexity: O(n)
#Space Complexity: O(1)



class Solution:
    maxint = 2147483647
    def myAtoi(self, s: str) -> int:
        s = s.strip()
        if len(s) == 0:
            return 0
        firstChar = s[0]
        sign = 1
        start = 0
        res = 0
        if firstChar == '+':
            sign = 1
            start += 1
        elif firstChar == '-':
            sign = -1
            start += 1
        for i in range(start,len(s)):
            if s[i].isdigit() == False:
                return int(res*sign)
            res = res*10 + ord(s[i])-ord('0')
            if sign == 1 and res > self.maxint:
                return self.maxint
            if sign == -1 and res > self.maxint:
                return -self.maxint-1
        return int(res*sign)

#Medium
#Time Complexity: O(n)
#Space Complexity: O(n)

#Visit by Row
class Solution:
    def convert(self, s: str, numRows: int) -> str:
        if numRows <= 1:
            return s
        stringBuilder = [""]*numRows
        for i in range(len(s)):
            index = i%(2*numRows-2)
            index = index if index<numRows else 2*numRows-2-index
            stringBuilder[index] += s[i]
        
        sb = "".join(stringBuilder)
        return sb


#Hard
#Very Important, Facebook
#Time Complexity O(min(M,N)) best case O(M*N) worst case
#Space Complexity O(1)
#Backtracking
class Solution:
    def isMatch(self, s: str, p: str) -> bool:
        s_len, p_len = len(s), len(p)
        s_idx = p_idx = 0
        star_idx = s_tmp_idx = -1
        
        while s_idx<s_len:
            if p_idx < p_len and p[p_idx] in ['?',s[s_idx]]:
                s_idx += 1
                p_idx += 1
            elif p_idx < p_len and p[p_idx] == '*':
                star_idx = p_idx
                s_tmp_idx = s_idx
                p_idx += 1
            elif star_idx == -1:
                return False
            else:
                p_idx = star_idx + 1
                s_idx = s_tmp_idx + 1
                s_tmp_idx = s_idx
        while p_idx < p_len and p[p_idx] == '*':
            p_idx += 1
        return p_idx == len(p)
        
#s_idx and p_idx indicate the indices of s and p. star_idx and s_tmp_idx indicate the indices of s and p when we meet "star".




#Hard
#Time Complexity O(log(m+n))
#Space Complexity O(1)

class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        size1,size2 = len(nums1),len(nums2)
        if size1>size2:
            return self.findMedianSortedArrays(nums2,nums1)
        k = (size1+size2+1)//2
        left = 0
        right = size1
        while left<right:
            m1 = left+(right-left)//2
            m2 = k-m1
            if nums1[m1] < nums2[m2-1]:
                left = m1+1
            else:
                right = m1
        m1 = left
        m2 = k-left
        c1 = max(-1000000 if m1<=0 else nums1[m1-1], -1000000 if m2<=0 else nums2[m2-1])
        if (size1+size2)%2!=0:
            return c1
        c2 = min(1000000 if m1>=size1 else nums1[m1], 1000000 if m2>=size2 else nums2[m2])
        return (c1+c2)/2.0
            
        

#Medium
#Very important

#Hash Set
#Time Complexity O(N^2)
#Space Complexity O(N^2)
class Solution:
    def isValidSudoku(self, board: List[List[str]]) -> bool:
        N = 9
        rows = [set() for _ in range(N)]
        cols = [set() for _ in range(N)]
        boxs = [set() for _ in range(N)]
        
        for r in range(N):
            for c in range(N):
                val = board[r][c]
                if val == '.':
                    continue
                if val in rows[r]:
                    return False
                rows[r].add(val)
                if val in cols[c]:
                    return False
                cols[c].add(val)
                #check the box
                idx = (r//3)*3+c//3
                if val in boxs[idx]:
                    return False
                boxes[idx].add(val)
        return True


#Bit Manipulation
#Time Complexity O(N^2)
#Space Complexity O(N)
class Solution:
    def isValidSudoku(self, board: List[List[str]]) -> bool:
        N = 9
        rows = [0]*N
        cols = [0]*N
        boxs = [0]*N
        for r in range(N):
            for c in range(N):
                if board[r][c] == ".":
                    continue
                pos = int(board[r][c])-1
                if rows[r]&(1<<pos):
                    return False
                rows[r] |= (1<<pos)
                if cols[c]&(1<<pos):
                    return False
                cols[c] |= (1<<pos)
                idx = (r//3)*3+c//3
                if boxs[idx]&(1<<pos):
                    return False
                boxs[idx] |= (1<<pos)
        return True

#Medium

#Time Complexity: O(N)
#Space Complexity: O(N)
class Solution:
    def verifyPreorder(self, preorder: List[int]) -> bool:
        stack = []
        mini = float('-inf')
        for num in preorder:
            if num < mini:
                return False
            while stack and num > stack[-1]:
                mini = stack.pop()
            stack.append(num)
        return True

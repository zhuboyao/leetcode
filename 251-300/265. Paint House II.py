#Hard

#Time Complexity: O(N*k)
#Space Complexity: O(1)
class Solution:
    def minCostII(self, costs: List[List[int]]) -> int:
        n = len(costs)
        if n == 0:
            return 0
        k = len(costs[0])
        for house in range(1,n):
            min_color = second_min_color = None
            for color in range(k):
                cost = costs[house-1][color]
                if min_color is None or cost < costs[house-1][min_color]:
                    second_min_color = min_color
                    min_color = color
                elif second_min_color is None or cost < costs[house-1][second_min_color]:
                    second_min_color = color
            for color in range(k):
                if color == min_color:
                    costs[house][color] += costs[house-1][second_min_color]
                else:
                    costs[house][color] += costs[house-1][min_color]
        return min(costs[-1])

#Easy

#Like BFS
#Time Complexity: O(N+E)
class Solution:
    def validTree(self, n: int, edges: List[List[int]]) -> bool:
        if len(edges) != n-1:
            return False
        adj_list = [[] for _ in range(n)]
        for A,B in edges:
            adj_list[A].append(B)
            adj_list[B].append(A)
        parent = {0:-1}
        stack = [0]
        while stack:
            node = stack.pop()
            for neighbor in adj_list[node]:
                if neighbor == parent[node]:
                    continue
                if neighbor in parent:
                    return False
                parent[neighbor] = node
                stack.append(neighbor)
        return len(parent)==n

#DFS
class Solution:
    def validTree(self, n: int, edges: List[List[int]]) -> bool:
        if len(edges) != n-1:
            return False
        adj_list = [[] for _ in range(n)]
        for A,B in edges:
            adj_list[A].append(B)
            adj_list[B].append(A)
        seen = set()
        self.dfs(0,seen,adj_list)
        return len(seen)==n
        
    def dfs(self,node,seen,adj_list):
        if node in seen:
            return
        seen.add(node)
        for neighbor in adj_list[node]:
            self.dfs(neighbor,seen,adj_list)

#Union Find
class Solution:
    def validTree(self, n: int, edges: List[List[int]]) -> bool:
        if n==1 and len(edges)==0:
            return True
        if n<1 or edges is None or len(edges) != n-1:
            return False
        roots = [-1]*n
        for A,B in edges:
            x = self.find(roots,A)
            y = self.find(roots,B)
            if x == y:
                return False
            roots[x] = y
        return True
    
    def find(self,roots,i):
        while roots[i] != -1:
            i = roots[i]
        return i
    

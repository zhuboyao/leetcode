#Medium
#Important


Time Complexity: O(N)
Space Complexity: O(N)
class Codec:
    def encode(self, strs: [str]) -> str:
        """Encodes a list of strings to a single string.
        """
        sb = ""
        for strval in strs:
            sb = sb + str(len(strval)) + "/" + str(strval)
        return sb

    def decode(self, s: str) -> [str]:
        """Decodes a single string to a list of strings.
        """
        res = []
        i = 0
        while i < len(s):
            slash = s.find('/',i)  #searching from i-th pos
            size = int(s[i:slash])
            res.append(s[slash+1:slash+1+size])
            i = slash+1+size
        return res


# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.decode(codec.encode(strs))

#Medium

#Time Complexity: O(NlogN)
#Space Complexity: O(1)
class Solution:
    def hIndex(self, citations: List[int]) -> int:
        citations.sort()
        res = 0
        while res < len(citations) and citations[len(citations)-1-res] > res:
            res += 1
        return res

#Time: O(N)
#Space: O(N)
class Solution:
    def hIndex(self, citations: List[int]) -> int:
        n = len(citations)
        papers = [0]*(n+1)
        for c in citations:
            index = min(c,n)
            papers[index] += 1
        k = 0
        for i in range(n,0,-1):
            k += papers[i]
            if k >= i:
                return i
        return 0

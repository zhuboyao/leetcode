#Easy


class Solution:
    def addDigits(self, num: int) -> int:
        res = 0
        while num > 0:
            remaining = num%10
            res += remaining
            num //= 10
            if num == 0 and res > 9:
                num = res
                res = 0
        return res


class Solution:
    def addDigits(self, num: int) -> int:
        if num == 0:
            return 0
        if num % 9 == 0:
            return 9
        return num % 9


class Solution:
    def addDigits(self, num: int) -> int:
        return 1 + (num - 1) % 9 if num else 0

#Easy

#Recursion
#Time Complexity: O(N)
#Space Complexity: O(N)
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def binaryTreePaths(self, root: Optional[TreeNode]) -> List[str]:
        res = []
        if not root:
            return []
        self.helper(res,root,"")
        return res
    
    def helper(self,res,root,path):
        if not root.left and not root.right:
            res.append(path+str(root.val))
        
        if root.left:
            self.helper(res,root.left,path+str(root.val)+'->')
        if root.right:
            self.helper(res,root.right,path+str(root.val)+'->')

#Iteration
class Solution:
    def binaryTreePaths(self, root: Optional[TreeNode]) -> List[str]:
        if not root:
            return []
        paths = []
        stack = [(root,str(root.val))]
        while stack:
            node,path = stack.pop()
            if not node.left and not node.right:
                paths.append(path)
            if node.left:
                stack.append((node.left,path+'->'+str(node.left.val)))
            if node.right:
                stack.append((node.right,path+'->'+str(node.right.val)))
        return paths

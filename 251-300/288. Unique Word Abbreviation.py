#Medium


class ValidWordAbbr:

    def __init__(self, dictionary: List[str]):
        self.maps = {}
        for s in dictionary:
            key = self.getKey(s)
            if key in self.maps:
                if self.maps[key] != s:
                    self.maps[key] = ""
            else:
                self.maps[key] = s

    def isUnique(self, word: str) -> bool:
        return self.getKey(word) not in self.maps or self.maps[self.getKey(word)] == word

    def getKey(self,s):
        if len(s) <= 2:
            return s
        return s[0] + str(len(s)-2) +s[-1]
    
    
# Your ValidWordAbbr object will be instantiated and called as such:
# obj = ValidWordAbbr(dictionary)
# param_1 = obj.isUnique(word)

#Medium

#Time Complexity:
#Space Complexity:
#Time Limit Exceeded
class Solution:
    def wallsAndGates(self, rooms: List[List[int]]) -> None:
        """
        Do not return anything, modify rooms in-place instead.
        """
        for i in range(len(rooms)):
            for j in range(len(rooms[0])):
                if rooms[i][j] == 0:
                    self.dfs(rooms,i,j,0)
                    
    def dfs(self,rooms,i,j,dis):
        if i < 0 or i >= len(rooms) or j < 0 or j >= len(rooms[0]) or rooms[i][j] < dis:
            return
        rooms[i][j] = dis
        self.dfs(rooms,i-1,j,dis+1)
        self.dfs(rooms,i+1,j,dis+1)
        self.dfs(rooms,i,j-1,dis+1)
        self.dfs(rooms,i,j+1,dis+1)



#BFS
#Time Complexity: O(M*N)
#Space Complexity: O(M*N)

class Solution:
    def wallsAndGates(self, rooms: List[List[int]]) -> None:
        """
        Do not return anything, modify rooms in-place instead.
        """
        if not rooms:
            return
        row, col = len(rooms), len(rooms[0])
        q = [(i,j) for i in range(row) for j in range(col) if rooms[i][j]==0]
        for x,y in q:
            distance = rooms[x][y] + 1
            directions = [(-1,0),(1,0),(0,1),(0,-1)]
            for dx, dy in directions:
                new_x, new_y = x+dx, y+dy
                if 0<=new_x<row and 0<=new_y<col and rooms[new_x][new_y]==2147483647:
                    rooms[new_x][new_y] = distance
                    q.append((new_x,new_y))

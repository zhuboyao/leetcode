#Easy

#Time Complexity: O(N)
#Space Complexity: O(N)
class Solution:
    def singleNumber(self, nums: List[int]) -> List[int]:
        hashmap = Counter(nums)
        return [x for x in hashmap if hashmap[x]==1]

#Space: O(1)
class Solution:
    def singleNumber(self, nums: List[int]) -> List[int]:
        bit = 0
        for num in nums:
            bit ^= num
        diff = bit&(-bit)
        x = 0
        for num in nums:
            if num&diff:
                x ^= num
        return [x,bit^x]
        

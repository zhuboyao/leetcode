#Hard
#270

#Time: O(NlogN)
#Space: O(N)
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def closestKValues(self, root: Optional[TreeNode], target: float, k: int) -> List[int]:
        def inorder(r):
            return inorder(r.left) + [r.val] + inorder(r.right) if r else []
        nums = inorder(root)
        nums.sort(key=lambda x: abs(x-target))
        return nums[:k]


#No sorting
#Time Complexity: O(N)
class Solution:
    def closestKValues(self, root: Optional[TreeNode], target: float, k: int) -> List[int]:
        queue = deque()
        self.helper(queue,root,target,k)
        return queue
    
    def helper(self,queue,root,target,k):
        if not root:
            return
        self.helper(queue,root.left,target,k)
        if len(queue) == k:
            if abs(target-root.val) < abs(target-queue[0]):
                queue.popleft()
            else:
                return
        queue.append(root.val)
        self.helper(queue,root.right,target,k)



#Time Complexity: O(logN)
class Solution:
    def closestKValues(self, root: Optional[TreeNode], target: float, k: int) -> List[int]:
        prev_stack, next_stack = [], []
        cur_node = root
        while cur_node:
            if cur_node.val < target:
                prev_stack.append(cur_node)
                cur_node = cur_node.right
            else:
                next_stack.append(cur_node)
                cur_node = cur_node.left
        res = []
        for _ in range(k):
            if not prev_stack and not next_stack:
                break
            elif not next_stack:
                res.append(self.getPrevVal(prev_stack))
            elif not prev_stack:
                res.append(self.getNextVal(next_stack))
            elif target-prev_stack[-1].val < next_stack[-1].val-target:
                res.append(self.getPrevVal(prev_stack))
            else:
                res.append(self.getNextVal(next_stack))
        return res
    
    def getPrevVal(self,prev_stack):
        cur_node = prev_stack.pop()
        next_node = cur_node.left
        while next_node:
            prev_stack.append(next_node)
            next_node = next_node.right
        return cur_node.val
    
    def getNextVal(self,next_stack):
        cur_node = next_stack.pop()
        next_node = cur_node.right
        while next_node:
            next_stack.append(next_node)
            next_node = next_node.left
        return cur_node.val

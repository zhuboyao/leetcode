#Medium

Time Complexity: O(N)
Space Complexity: O(N)
class Solution:
    def nthUglyNumber(self, n: int) -> int:
        nums = [0]*(n-1)
        index2 = 0
        index3 = 0
        index5 = 0
        nums[0] = 1
        for i in range(1,n):
            nums[i] = min(nums[index2]*2,nums[index3]*3,nums[index5]*5)
            if nums[i] == nums[index2]*2:
                index2 += 1
            if nums[i] == nums[index3]*3:
                index3 += 1
            if nums[i] == nums[index5]*5:
                index5 += 1
        return nums[n-1]

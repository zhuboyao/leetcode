#Medium
#DP


#Time Complexity: O(N/sqrt(N))
#Space Complexity: O(N)
class Solution:
    def numSquares(self, n: int) -> int:
        res = [sys.maxsize]*(n+1)
        res[0] = 0
        for i in range(1,n+1):
            j = 1
            while j*j<=i:
                res[i] = min(res[i],res[i-j*j]+1)
                j += 1
        return res[n]

#Hard

#Time Complexity: O(N*4^N)
#Space Complexity: O(N)
class Solution:
    def addOperators(self, num: str, target: int) -> List[str]:
        res = []
        if len(num) == 0:
            return res
        self.helper(res,"",num,target,0,0,0)
        return res
    
    def helper(self,res,path,num,target,pos,val,pre):
        if pos == len(num):
            if target == val:
                res.append(path)
                return
        for i in range(pos,len(num)):
            if i != pos and num[pos] == "0":
                break
            cur = int(num[pos:i+1])
            if pos == 0:
                self.helper(res,path+str(cur),num,target,i+1,cur,cur)
            else:
                self.helper(res,path+"+"+str(cur),num,target,i+1,val+cur,cur)
                self.helper(res,path+"-"+str(cur),num,target,i+1,val-cur,-cur)
                self.helper(res,path+"*"+str(cur),num,target,i+1,val-pre+pre*cur,pre*cur)

#Medium

#Time Complexity: O((N/2+1)!)
#Space Complexity: O(N)
class Solution:
    def generatePalindromes(self, s: str) -> List[str]:
        ans = []
        n = len(s)
        counter = Counter(s)
        
        def helper(tmp):
            if len(tmp) == n:
                ans.append(tmp)
            for k,v in counter.items():
                if v > 0:
                    counter[k] -= 2
                    helper(k+tmp+k)
                    counter[k] += 2
        
        odd = [key for key,value in counter.items() if value%2 != 0]
        if len(odd) > 1:
            return []
        if len(odd) == 1:
            counter[odd[0]] -= 1
            helper(odd[0])
        else:
            helper("")
        return ans

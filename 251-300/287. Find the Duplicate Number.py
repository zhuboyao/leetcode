#Medium
#Google
#142
#Time Complexity: O(NlogN)
#Space Complexity: O(1)
class Solution:
    def findDuplicate(self, nums: List[int]) -> int:
        low, high = 0, len(nums)-1
        while low <= high:
            mid = (low+high)//2
            count = 0
            count = sum(num<=mid for num in nums)
            if count > mid:
                duplicate = mid
                high = mid-1
            else:
                low = mid+1
        return duplicate

#Time Complexity: O(N)
#Space Complexity: O(1)
class Solution:
    def findDuplicate(self, nums: List[int]) -> int:
        slow = nums[0]
        fast = nums[nums[0]]
        while slow != fast:
            slow = nums[slow]
            fast = nums[nums[fast]]
        fast = 0
        while slow != fast:
            slow = nums[slow]
            fast = nums[fast]
        return fast

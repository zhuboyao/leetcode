#Medium

#Time Complexity:
#Space Complexity:
class ZigzagIterator:
    def __init__(self, v1: List[int], v2: List[int]):
        self.v1 = v1[::-1]
        self.v2 = v2[::-1]
        self.zz = 1  #bool: if 1 pop from v1, if 2 pop from v2
        
    def next(self) -> int:
        if self.zz == 1 and len(self.v1) != 0:
            self.zz = 2
            return self.v1.pop()
        else:
            self.zz = 1
            if len(self.v2) != 0:
                return self.v2.pop()
            else:
                return self.v1.pop()

    def hasNext(self) -> bool:
        return True if len(self.v1) + len(self.v2) != 0 else False

# Your ZigzagIterator object will be instantiated and called as such:
# i, v = ZigzagIterator(v1, v2), []
# while i.hasNext(): v.append(i.next())

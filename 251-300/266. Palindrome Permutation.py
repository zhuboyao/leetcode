#Easy

#Time Complexity: O(N)
#Space Complexity: O(1)
class Solution:
    def canPermutePalindrome(self, s: str) -> bool:
        HashMap = defaultdict(int)
        for i in range(len(s)):
            HashMap[s[i]] += 1
        count = 0
        for n in HashMap.values():
            count += n%2
        return count <= 1


class Solution:
    def canPermutePalindrome(self, s: str) -> bool:
        SET = set()
        for c in s:
            if c in SET:
                SET.remove(c)
            else:
                SET.add(c)
        return len(SET)<=1

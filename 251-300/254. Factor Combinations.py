#Medium


class Solution:
    def getFactors(self, n: int) -> List[List[int]]:
        res = []
        self.helper(res,[],n,2)
        return res
    
    def helper(self,res,arr,n,prev_factor):
        for i in range(prev_factor,int(sqrt(n))+1):
            if n%i == 0:
                res.append(arr+[i,n//i])
                self.helper(res,arr+[i],n//i,i)


#Time Limit Exceeded
class Solution:
    def getFactors(self, n: int) -> List[List[int]]:
        res = []
        self.helper(res,[],n,2)
        return res
    
    def helper(self,res,lists,n,start):
        if n == 1:
            if len(lists) > 1:
                res.append(lists)
                return
        i = start
        while i <= n:
            if n%i == 0:
                lists.append(i)
                self.helper(res,lists[:],n//i,i)
                lists = lists[:-1]
            i += 1
                

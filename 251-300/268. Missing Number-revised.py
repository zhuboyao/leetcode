#Easy


#Sorting

#Hashset
#Time Complexity: O(N)
#Space Complexity: O(N)
class Solution:
    def missingNumber(self, nums: List[int]) -> int:
        num_set = set(nums)
        n = len(nums)+1
        for number in range(n):
            if number not in num_set:
                return number

#Gauss
#Space Complexity: O(1)
class Solution:
    def missingNumber(self, nums: List[int]) -> int:
        expected_sum = len(nums)*(len(nums)+1)//2
        actual_sum = sum(nums)
        return expected_sum-actual_sum

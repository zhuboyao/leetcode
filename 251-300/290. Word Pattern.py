#Easy
#205
#important

#Time Complexity: O(N), N is the number of words in s
#Space Complexity: O(M), M is the number of unique words in s
class Solution:
    def wordPattern(self, pattern: str, s: str) -> bool:
        map_char = {}
        map_word = {}
        words = s.split(" ")
        if len(words) != len(pattern):
            return False
        
        for c,w in zip(pattern,words):
            if c not in map_char:
                if w in map_word:
                    return False
                else:
                    map_char[c] = w
                    map_word[w] = c
            else:
                if map_char[c] != w:
                    return False
        return True

#Single Map
class Solution:
    def wordPattern(self, pattern: str, s: str) -> bool:
        arr = s.split()
        if len(arr) != len(pattern):
            return False
        maps = {}
        for i in range(len(arr)):
            c = pattern[i]
            if c in maps:
                if maps[c] == arr[i]:
                    continue
                else:
                    return False
            else:
                if arr[i] not in maps.values():
                    maps[c] = arr[i]
                else:
                    return False
        return True

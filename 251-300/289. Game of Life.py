#Medium

#Time: O(M*N)
#Space: O(1)
class Solution:
    def gameOfLife(self, board: List[List[int]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        neighbors = [(1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1),(0,1),(1,1)]
        rows = len(board)
        cols = len(board[0])
        for row in range(rows):
            for col in range(cols):
                live_neighbors = 0
                for neighbor in neighbors:
                    r = row+neighbor[0]
                    c = col+neighbor[1]
                    if (r<rows and r>=0) and (c<cols and c>=0) and abs(board[r][c]) == 1:
                        live_neighbors += 1
                
                # Rule1 or Rule3
                if board[row][col] == 1 and (live_neighbors < 2 or live_neighbors > 3):
                    board[row][col] = -1 # is dead now but was live
                # Rule4
                if board[row][col] == 0 and live_neighbors == 3:
                    board[row][col] = 2 #is live now but was dead
        
        for row in range(rows):
            for col in range(cols):
                if board[row][col] > 0:
                    board[row][col] = 1
                else:
                    board[row][col] = 0

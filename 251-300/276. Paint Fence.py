#Medium
#DP


#Time Complexity: O(N)
#Space Complexity: O(1)
class Solution:
    def numWays(self, n: int, k: int) -> int:
        if n==1:
            return k
        two_back = k
        one_back = k*k
        
        for i in range(3,n+1):
            curr = (k-1)*(one_back+two_back)
            two_back = one_back
            one_back = curr
        return one_back

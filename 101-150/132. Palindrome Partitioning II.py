#Hard
#Time Complexity: O(N^2)
#Space Complexity: O(N)

class Solution:
    def minCut(self, s: str) -> int:
        cutsDp = [i for i in range(len(s))]
        for mid in range(len(s)):
            self.findMinimumCuts(mid,mid,cutsDp,s)
            self.findMinimumCuts(mid-1,mid,cutsDp,s)
        return cutsDp[len(s)-1]
    
    def findMinimumCuts(self,startIndex,endIndex,cutsDp,s):
        start = startIndex
        end = endIndex
        while start >= 0 and end < len(s) and s[start]==s[end]:
            if start == 0:
                newCut = 0
            else:
                newCut = cutsDp[start-1]+1
            cutsDp[end] = min(cutsDp[end],newCut)
            start -= 1
            end += 1
        

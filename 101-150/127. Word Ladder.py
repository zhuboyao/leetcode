#Easy

Very Important, as well as 126



class Solution:
    def ladderLength(self, beginWord: str, endWord: str, wordList: List[str]) -> int:
        s = set(wordList)
        if beginWord in s:
            s.remove(beginWord)
        queue = deque([beginWord])
        level = 1
        curNum = 1
        nextNum = 0
        while queue:
            word = queue.popleft()
            curNum -= 1
            for i in range(len(word)):
                for j in "abcdefghijklmnopqrstuvwxyz":
                    temp = word[:i]+j+word[i+1:]
                    if temp in s:
                        if temp == endWord:
                            return level + 1
                        nextNum += 1
                        queue.append(temp)
                        s.remove(temp)
            if curNum == 0:
                curNum = nextNum
                nextNum = 0
                level += 1
        return 0



class Solution:
    def ladderLength(self, beginWord: str, endWord: str, wordList: List[str]) -> int:
        s = set(wordList)
        if beginWord in s:
            s.remove(beginWord)
        queue = deque([beginWord])
        Map = {beginWord:1}
        while queue:
            word = queue.popleft()
            curLevel = Map[word]
            for i in range(len(word)):
                for j in "abcdefghijklmnopqrstuvwxyz":
                    temp = word[:i]+j+word[i+1:]
                    if temp in s:
                        if temp == endWord:
                            return curLevel + 1
                        Map[temp] = curLevel + 1
                        queue.append(temp)
                        s.remove(temp)
        return 0
        

#Hard
#Very often in interviews

#Not really understand
class Solution:
    def __init__(self):
        self.mapping = {}
    def wordBreak(self, s: str, wordDict: List[str]) -> List[str]:
        return self.dfs(s,wordDict,0)
    def dfs(self,s,wordDict,start):
        if start in self.mapping:
            return self.mapping[start]
        res = []
        if start == len(s):
            res.append("")
        for end in range(start+1,len(s)+1):
            if s[start:end] in wordDict:
                list1 = self.dfs(s,wordDict,end)
                for temp in list1:
                    split = ""
                    if temp != "":
                        split = " "
                    res.append(s[start:end]+split+temp)
        self.mapping[start] = res
        return res



#Time Complexity: O(N^2+2^N+W)
#Space Complexity: O(N^2+N*2^N+W)
class Solution:
    def wordBreak(self, s: str, wordDict: List[str]) -> List[str]:
        if set(Counter(s).keys()) > set(Counter("".join(wordDict)).keys()):
            return []
        wordSet = set(wordDict)
        dp = [[]]*(len(s)+1)
        dp[0] = [""]
        for endIndex in range(1,len(s)+1):
            sublist = []
            for startIndex in range(0,endIndex):
                word = s[startIndex:endIndex]
                if word in wordSet:
                    for subsentence in dp[startIndex]:
                        sublist.append((subsentence+" "+word).strip())
            dp[endIndex] = sublist
        return dp[len(s)]

#Easy

#Time: O(N)
#Space: O(N)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def minDepth(self, root: Optional[TreeNode]) -> int:
        if not root:
            return 0
        if not root.left or not root.right:
            return max(self.minDepth(root.left),self.minDepth(root.right))+1
        return min(self.minDepth(root.left),self.minDepth(root.right))+1


class Solution:
    def minDepth(self, root):
        if not root:
            return 0
        else:
            node_deque = deque([(1, root),])
        
        while node_deque:
            depth, root = node_deque.popleft()
            children = [root.left, root.right]
            if not any(children):
                return depth
            for c in children:
                if c:
                    node_deque.append((depth + 1, c))
# if not any(children): means if there are not two children, we return depth. good.

#Medium
#important

#Time Complexity: O(N)
#Space Complexity: O(1) or O(h)
"""
# Definition for a Node.
class Node:
    def __init__(self, val: int = 0, left: 'Node' = None, right: 'Node' = None, next: 'Node' = None):
        self.val = val
        self.left = left
        self.right = right
        self.next = next
"""

class Solution:
    def connect(self, root: 'Optional[Node]') -> 'Optional[Node]':
        if not root:
            return
        if root.left:
            root.left.next = root.right
        if root.next and root.right:
            root.right.next = root.next.left
        self.connect(root.left)
        self.connect(root.right)
        return root


#Space: O(1)
class Solution:
    def connect(self, root: 'Optional[Node]') -> 'Optional[Node]':
        leftmost = root
        while leftmost:
            head = leftmost
            while head:
                if head.left:
                    head.left.next = head.right
                if head.right and head.next:
                    head.right.next = head.next.left
                head = head.next
            leftmost = leftmost.left
        return root

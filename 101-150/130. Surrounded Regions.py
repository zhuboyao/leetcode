#Medium

#Time Complexity: O(N), N the number of cells
#Space Complexity: O(N), N the number of cells
class Solution:
    def solve(self, board: List[List[str]]) -> None:
        """
        Do not return anything, modify board in-place instead.
        """
        m = len(board)
        n = len(board[0])
        if len(board) == 0:
            return
        for i in range(m):
            if board[i][0] == "O":
                self.dfs(board,i,0)
            if board[i][n-1] == "O":
                self.dfs(board,i,n-1)
        for i in range(n):
            if board[0][i] == "O":
                self.dfs(board,0,i)
            if board[m-1][i] == "O":
                self.dfs(board,m-1,i)
        for i in range(m):
            for j in range(n):
                board[i][j] = "O" if board[i][j] == "1" else "X"
                
    def dfs(self,board,i,j):
        m = len(board)
        n = len(board[0])
        if i<0 or j<0 or i>m-1 or j>n-1 or board[i][j] != "O":
            return
        board[i][j] = "1"
        self.dfs(board,i,j+1)
        self.dfs(board,i,j-1)
        self.dfs(board,i+1,j)
        self.dfs(board,i-1,j)

#Medium
#Refer to 116, second approach

"""
# Definition for a Node.
class Node:
    def __init__(self, val: int = 0, left: 'Node' = None, right: 'Node' = None, next: 'Node' = None):
        self.val = val
        self.left = left
        self.right = right
        self.next = next
"""
#Time: O(N)
#Space: O(1)
class Solution:
    def connect(self, root: 'Node') -> 'Node':
        if not root:
            return root
        leftmost = root
        while leftmost:
            prev,curr = None, leftmost
            leftmost = None
            while curr:
                prev,leftmost = self.processChild(curr.left,prev,leftmost)
                prev,leftmost = self.processChild(curr.right,prev,leftmost)
                curr = curr.next
        return root
    
    
    def processChild(self,childNode,prev,leftmost):
        if childNode:
            if prev:
                prev.next = childNode
            else:
                leftmost = childNode
            prev = childNode
        return prev,leftmost
    
    
#Space: O(N)
class Solution:
    def connect(self, root: 'Node') -> 'Node':
        if not root:
            return root
        Q = collections.deque([root])
        while Q:
            size = len(Q)
            for i in range(size):
                node = Q.popleft()
                if i < size-1:
                    node.next = Q[0]
                if node.left:
                    Q.append(node.left)
                if node.right:
                    Q.append(node.right)
        return root

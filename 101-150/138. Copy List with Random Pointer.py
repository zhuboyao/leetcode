#Medium

#Time O(N)
#Space O(N)

"""
# Definition for a Node.
class Node:
    def __init__(self, x: int, next: 'Node' = None, random: 'Node' = None):
        self.val = int(x)
        self.next = next
        self.random = random
"""

class Solution:
    def copyRandomList(self, head: 'Optional[Node]') -> 'Optional[Node]':
        if not head:
            return head
        cur = head
        while cur:  # 1->1'->2->2'->3->3'..
            next_ = cur.next
            copy = Node(cur.val,None,None)
            cur.next = copy
            copy.next = next_
            cur = next_
        cur = head
        while cur:
            if cur.random:
                cur.next.random = cur.random.next
            cur = cur.next.next
        cur = head
        dummy = Node(0,None,None)
        copycur = dummy
        while cur:
            next_ = cur.next.next
            copy = cur.next
            copycur.next = copy
            copycur = copy
            cur.next = next_
            cur = next_
        return dummy.next
            
            
            
            
            



#Medium

#Time: O(N)
#Space: O(N)
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def zigzagLevelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:
        res = []
        if not root:
            return res
        queue = deque([root])
        left = True
        while queue:
            size = len(queue)
            level = deque([])
            for i in range(size):
                cur = queue.popleft()
                if left:
                    level.append(cur.val)
                else:
                    level.appendleft(cur.val)
                if cur.left:
                    queue.append(cur.left)
                if cur.right:
                    queue.append(cur.right)
            res.append(level)
            left = not left
        return res
                

#Same Complexity

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def zigzagLevelOrder(self, root: Optional[TreeNode]) -> List[List[int]]:
        if root is None:
            return []
        res = []
        self.dfs(root,res,0)
        return res
    
    def dfs(self,node,res,level):
        if level==len(res):
            res.append(deque([node.val]))
        else:
            if level%2 == 0:
                res[level].append(node.val)
            else:
                res[level].appendleft(node.val)
        for next_node in [node.left,node.right]:
            if next_node:
                self.dfs(next_node,res,level+1)
                

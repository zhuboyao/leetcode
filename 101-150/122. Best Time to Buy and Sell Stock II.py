#Medium

#Time: O(N)
#Space: O(1)
class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        if not prices or len(prices)<2:
            return 0
        profit = 0
        for i in range(1,len(prices)):
            if prices[i-1] < prices[i]:
                profit += prices[i]-prices[i-1]
        return profit

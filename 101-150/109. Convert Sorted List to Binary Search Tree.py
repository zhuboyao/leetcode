#Medium

#Similar to 108, array->BST, Linked List->BST


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def sortedListToBST(self, head: Optional[ListNode]) -> Optional[TreeNode]:
        if not head:
            return None
        return self.toBST(head,None)
    
    def toBST(self,head,tail):
        if head == tail:
            return None
        slow = head
        fast = head
        while fast != tail and fast.next != tail:
            fast = fast.next.next
            slow = slow.next
        root=TreeNode(slow.val)
        root.left = self.toBST(head,slow)
        root.right = self.toBST(slow.next,tail)
        return root
    
#Time Complexity: O(N)
#Space Complexity: O(N)

#Medium

#preorder = [3,9,20,15,7]             3
#inorder  = [9,3,15,20,7]            / \
#                                   9  20
#                                      / \
#                                     15  7

#Time Complexity: O(N^2)
#Space Complexity: O(N)
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
# Not very good
class Solution:
    def buildTree(self, preorder: List[int], inorder: List[int]) -> Optional[TreeNode]:
        if len(preorder) == 0:
            return
        root = TreeNode(preorder[0])
        for i in range(len(inorder)):
            if preorder[0] == inorder[i]:
                break
        root.left = self.buildTree(preorder[1:i+1],inorder[0:i])
        root.right = self.buildTree(preorder[i+1:],inorder[i+1:])
        return root
        

#Time Complexity: O(N)
#Space Complexity: O(N)
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    
    def buildTree(self, preorder: List[int], inorder: List[int]) -> Optional[TreeNode]:
        def array_to_tree(left, right):
            nonlocal preorder_index
            if left > right: return None

            root_value = preorder[preorder_index]
            root = TreeNode(root_value)

            preorder_index += 1

            root.left = array_to_tree(left, inorder_index_map[root_value] - 1)
            root.right = array_to_tree(inorder_index_map[root_value] + 1, right)

            return root

        preorder_index = 0

        inorder_index_map = {}
        for index, value in enumerate(inorder):
            inorder_index_map[value] = index

        return array_to_tree(0, len(preorder) - 1)



#Time Complexity: O(N)
#Space Complexity: O(N)
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    hashMap = {}
    def buildTree(self, preorder: List[int], inorder: List[int]) -> Optional[TreeNode]:
        for i in range(len(inorder)):
            self.hashMap[inorder[i]] = i
        return self.helper(0,0,len(inorder)-1,preorder,inorder)
    
    def helper(self,preStart,inStart,inEnd,preorder,inorder):
        if preStart > len(preorder)-1 or inStart > inEnd:
            return
        root = TreeNode(preorder[preStart])
        inIndex = self.hashMap[root.val]
        root.left = self.helper(preStart+1,inStart,inIndex-1,preorder,inorder)
        root.right = self.helper(preStart+inIndex-inStart+1,inIndex+1,inEnd,preorder,inorder)
        return root
#Like this

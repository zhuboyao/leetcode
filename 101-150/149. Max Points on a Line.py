#Hard


#Time Complexity: O(N^3)
#Space Complexity: O(1)
class Solution:
    def maxPoints(self, points: List[List[int]]) -> int:
        res = 1
        size = len(points)
        if size < 3:
            return size
        for i in range(size-2):
            for j in range(i+1,size-1):
                cnt = 2
                x1,y1 = points[i][0],points[i][1]
                x2,y2 = points[j][0],points[j][1]
                for k in range(j+1,size):
                    x3,y3 = points[k][0],points[k][1]
                    if x1*y2 + x2*y3 + x3*y1 - x3*y2 - x2*y1 - x1*y3 == 0:
                        cnt += 1
                res = max(res,cnt)
        
        return res

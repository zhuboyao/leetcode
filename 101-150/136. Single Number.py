#Easy

#Time Complexity: O(N)
#Space Complexity: O(1)
class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        res = 0
        for i in range(len(nums)):
            res ^= nums[i]
        return res


#O(N)
#O(N)
class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        return 2*sum(set(nums))-sum(nums)

#O(N)
#O(N)
class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        hash_table = defaultdict(int)
        for num in nums:
            hash_table[num] += 1
        for num in nums:
            if hash_table[num] == 1:
                return num

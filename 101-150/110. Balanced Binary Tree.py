#Easy

#Time: O(N)
#Space: O(N)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    max_diff = 0
    def isBalanced(self, root: Optional[TreeNode]) -> bool:
        if not root:
            return True
        self.helper(root)
        return self.max_diff<=1
    
    def helper(self,node):
        if not node:
            return 0
        left = self.helper(node.left)
        right = self.helper(node.right)
        diff = abs(right-left)
        self.max_diff = max(self.max_diff,diff)
        return max(left,right)+1
    

#Medium
#Allow only constant space

#Time Complexity: O(N)
#Space Complexity: O(1)
class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        ones, twos = 0,0
        for num in nums:
            ones = (ones^num)&~twos
            twos = (twos^num)&~ones
        return ones

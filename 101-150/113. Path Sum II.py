#Medium

#Time Complexity: O(N)
#Space Complexity: O(N)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def pathSum(self, root: Optional[TreeNode], targetSum: int) -> List[List[int]]:
        res = []
        if not root:
            return res
        self.helper(res,[],root,targetSum)
        return res
    
    def helper(self,res,path,root,target):
        if not root:
            return
        path.append(root.val)
        if not root.left and not root.right:
            if target == root.val:
                res.append(list(path))
        self.helper(res,path,root.left,target-root.val)
        self.helper(res,path,root.right,target-root.val)
        path.pop()
    



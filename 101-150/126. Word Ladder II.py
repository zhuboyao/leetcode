#Hard

#Very Very Important, Often
#BFS + DFS
#undirected graph -> BFS -> Tree -> DFS

class Solution:
    def findLadders(self, beginWord: str, endWord: str, wordList: List[str]) -> List[List[str]]:
        self.res = []
        if len(wordList) == 0:
            return self.res
        curNum = 1
        nextNum = 0
        found = False
        
        queue = []
        unvisited = wordList
        visited = []
        self.Map = {}
        
        queue.append(beginWord)
        while queue:
            word = queue.pop(0)
            curNum -= 1
            for i in range(len(word)):
                builder = list(word)
                for ch in range(ord("a"),ord("z")+1):
                    builder[i] = chr(ch)
                    newWord = "".join(builder)
                    if newWord in unvisited:
                        if newWord not in visited:
                            visited.append(newWord)
                            nextNum += 1
                            queue.append(newWord)
                        if newWord in self.Map.keys():
                            self.Map[newWord].append(word)
                        else:
                            self.Map[newWord] = [word]
                        if newWord == endWord:
                            found = True
            if curNum == 0:
                if found:
                    break
                curNum = nextNum
                nextNum = 0
                for item in visited:
                    if item in unvisited:
                        unvisited.remove(item)
                visited = []
        
        def dfs(List,word,start):
            if word == start:
                List.insert(0,start)
                self.res.append(List[:])
                List.pop(0)
                return
            List.insert(0,word)
            if word in self.Map.keys():
                for s in self.Map[word]:
                    dfs(List,s,start)
            List.pop(0)
        
        dfs([],endWord,beginWord)
        return self.res

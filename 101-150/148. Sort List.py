#Medium

# Time: O(NlogN)
# Space: O(logN)

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def sortList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if not head or not head.next:
            return head
        middle = self.getMiddle(head)
        nextval = middle.next
        middle.next = None
        return self.merge(self.sortList(head),self.sortList(nextval))
        
    def getMiddle(self,head):
        slow = head
        fast = head
        while fast.next and fast.next.next:
            slow = slow.next
            fast = fast.next.next
        return slow
    
    def merge(self,a,b):
        dummy = ListNode(0)
        curr = dummy
        while a and b:
            if a.val <= b.val:
                curr.next = a
                a = a.next
            else:
                curr.next = b
                b = b.next
            curr = curr.next
        if not a:
            curr.next = b
        else:
            curr.next = a
        return dummy.next



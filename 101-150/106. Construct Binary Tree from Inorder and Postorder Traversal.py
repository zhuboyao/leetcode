#Mediu

#Time: O(n)
#Space: O(n)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    hashMap = {}
    def buildTree(self, inorder: List[int], postorder: List[int]) -> Optional[TreeNode]:
        for i in range(len(inorder)):
            self.hashMap[inorder[i]] = i
        return self.helper(len(postorder)-1,0,len(inorder)-1,postorder,inorder)
    
    def helper(self,postEnd,inStart,inEnd,postorder,inorder):
        if postEnd < 0 or inStart > inEnd:
            return
        root = TreeNode(postorder[postEnd])
        inIndex = self.hashMap[root.val]
        root.right = self.helper(postEnd-1,inIndex+1,inEnd,postorder,inorder)
        root.left = self.helper(postEnd-inEnd+inIndex-1,inStart,inIndex-1,postorder,inorder)
        return root


#Medium
#Refer to 102
#Time Complexity O(N)
#Space Complexity O(N)

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def levelOrderBottom(self, root: Optional[TreeNode]) -> List[List[int]]:
        levels = deque()
        if not root:
            return levels
        queue = deque([root])
        while queue:
            size = len(queue)
            level = []
            for i in range(size):
                cur = queue.popleft()
                if cur.left:
                    queue.append(cur.left)
                if cur.right:
                    queue.append(cur.right)
                level.append(cur.val)
            levels.appendleft(level)
        return levels


# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def levelOrderBottom(self, root: Optional[TreeNode]) -> List[List[int]]:
        levels = []
        if not root:
            return levels
        self.helper(root,levels,0)
        return levels[::-1]
        
    def helper(self,node,levels,level):
        if len(levels) == level:
            levels.append([])
        levels[level].append(node.val)
        if node.left:
            self.helper(node.left,levels,level+1)
        if node.right:
            self.helper(node.right,levels,level+1)

#Medium

#Time: (N^2)
#Space: (N)
class Solution:
    def minimumTotal(self, triangle: List[List[int]]) -> int:
        res = [0 for _ in range(len(triangle)+1)]
        for i in range(len(triangle)-1,-1,-1):
            for j in range(len(triangle[i])):
                res[j] = min(res[j],res[j+1])+triangle[i][j]
        return res[0]


